var express = require('express');
var app = express();
var http = require('http').Server(app);
var bodyParser = require('body-parser');

//在线
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static(__dirname));
// dbwrap.db_init();

app.all('*', function (req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,token,Accept");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", 'BIM');
    next();
});
http.listen(9080, function () {
    console.log('listening on *:9080');
});
