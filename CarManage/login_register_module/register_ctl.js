angular.module('myApp.LoginAndRegister')
.component('registerComponent',{
    templateUrl:'login_register_module/register.html',
    css:'login_register_module/register.css',
    controller: function ($scope, $http, $state) {
    console.log("This is register component")
    //弹出键盘覆盖输入框bug
    $('.form-input').focus(() => {
        console.log(">>>>>>")
        $('.footer').css('z-index', '-1');
    });
    // $('.form-input').blur(() => {
    //     console.log("<<<<<<<")
    //     $('.footer').css('z-index', '999');
    // })

        $scope.op={
            id:'',
            pass:''
        }
        // var idReg = new RegExp("^1[3|4|5|7|8][0-9]{9}$");
        var idReg = /^[a-zA-Z0-9_-]{6,12}$/;
        // var passReg= /^.*(?=.{6,})(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*? ]).*$/;
        var passReg= /^[0-9]{6,}$/;
        
        $scope.passConfirm='';
        $scope.goRegist=function(){
            if ($scope.op.id === null || $scope.op.id==='') {
                layer.msg(
                    "用户名不能为空",
                    {
                        icon:7,
                        time:3000
                    });
                    return;
            } else if (idReg.test($scope.op.id) === false) {
                layer.msg(
                    "用户名由6-12位的数字字母下划线短横杠组成", {
                        icon: 5,
                        time: 4000
                    });
                return;
            } else if ($scope.op.pass === null || $scope.op.pass === '') {
                layer.msg(
                    "密码不能为空", {
                        icon: 7,
                        time: 3000
                    });
                return;
            } else if (!passReg.test($scope.op.pass)) {
                layer.msg(
                    "密码为纯数字，至少6位", {
                        icon: 7,
                        time: 3000
                    });
                return;
            }else if ($scope.passConfirm === null || $scope.passConfirm === '') {
                layer.msg(
                    "请再次输入密码", {
                        icon: 7,
                        time: 3000
                    },
                    function (index) {
                        layer.close(index);
                    }
                );
                return;
            }else if($scope.op.pass!==$scope.passConfirm){
                layer.msg(
                    "两次输入的密码不一致", {
                        icon: 7,
                        time: 3000
                    },
                    function (index) {
                        layer.close(index);
                    }
                );
                return;
            }else{
                // console.log("输入的是个手机号")
                $http.post(baseurl + '/userregist', $scope.op).then(
                    (res)=>{
                        console.log(res.data)
                        if(res.data.ret===1){
                            layer.msg(
                                "注册成功！",
                                {
                                    icon:1,
                                    time:2000,
                                },
                                (index)=>{
                                    $state.go('login');
                                    layer.close(index);
                                }
                            )
                        }else if(res.data.ret===-1){
                            if (res.data.errcode===3){
                                layer.msg(
                                    "用户名已存在，换一个试试..", {
                                        icon: 5,
                                        time: 2000
                                    },
                                    function (index) {
                                        $state.reload();
                                        layer.close(index);
                                    }
                                )
                            }else{
                                layer.msg(
                                    "服务器忙，请稍候再试..", {
                                        icon: 7,
                                        time: 2000
                                    },
                                    function (index) {
                                        $state.reload();
                                        layer.close(index);
                                    }
                                )
                            }
                        }else if(res.data.ret===-3){
                            layer.msg(
                                "服务器忙，请稍候再试..", {
                                    icon: 7,
                                    time: 2000
                                },
                                function (index) {
                                    $state.reload();
                                    layer.close(index);
                                }
                            )
                        }
                    },
                    (err)=>{
                        console.log(err);
                        layer.msg(
                            "服务器忙，请稍候再试..",
                            {
                                icon:7,
                                time:2000
                            },
                            function(index){
                                layer.close(index);
                            }
                        )
                    }
                    
                )
            }
        }
    }
})