//模块依赖auth服务,所有与组件无关的逻辑都应该放入服务中
angular.module('myApp.LoginAndRegister', [
    'ui.router',
    'angularCSS',
    'myApp.server'
])
.config(function($stateProvider, $locationProvider){

    $stateProvider.state('login',{
        url:'/login',
        component:'loginComponent',
    });
    $stateProvider.state('register',{
        url:'/register',
        component:'registerComponent',
    });
    $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix("");
})