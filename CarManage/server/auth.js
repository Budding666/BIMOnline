angular.module('myApp.server', [
    'ui.router',
    'ngCookies'
])
.service('auth', function($http, $location, $state, $cookieStore){
    /*
    * 需要作出的判断.
    *  只需要判断他的session是否有效,如果有效，则认为是登录的
    *  可以通过如下方式去判断：
    *
    **/
    this.isLogged = function(ret){
        //登录成功以后会在本地存储cookie
        //判断cookie的条件是否满足以及session没有过期的时候才会去跳转
        if( $cookieStore.get('userid') != null){
            //判断会话是否过期
            if(ret == -3){
                $state.go('login')
            }
            return;
        }
        $state.go('login')
    }

    this.login = function (credentials){
        //从服务器获取数据
        console.log(baseurl_login + '/userlogin')

        $http.post(baseurl_login + '/userlogin', credentials).then((res) => {

            console.log(res)
            $cookieStore.put('userid', credentials.id)
            //存储登录角色信息
            if (res.data.ret == 1) {
                localStorage.setItem('car_role', res.data.info.car_role);
                if (res.data.info.car_role === "1") {
                    window.location.href = "#/home"; //跳转到实时数据页面
                } else if (res.data.info.car_role === "2") {
                    window.location.href = "#/home2";
                }
            } else {
                layer.msg("用户名或密码错误！",{icon:4});
                return
            }
        })
    }

})