angular.module('myApp')
.component('homeComponent',{
    templateUrl:'home/home.html',
    css:'home/home.css',
    controller:'homeCtl'
})
.controller('homeCtl',function($scope,$state){
    // console.log(people)
    console.log("This is home controller")
    // console.log(auth)
    // console.log(auth)
    // auth.isLogged();
    //跳出车管家去中达bim主页
    $scope.goHome=function(){
        window.location.href = URL.baseurl + 　'index1.html';
    }
    $scope.constList = [
        { id: 1, img: "real_data.png", name: "实时数据", url: "budProject.html" },
        { id: 2, img: "real_data.png", name: "分析图表", url: "deviceList.html" },
        { id: 3, img: "real_data.png", name: "录入信息", url: "daily.html" },
        { id: 4, img: "real_data.png", name: "维修记录", url: "agreement.html" }
    ];

    function getUrlParam(key) {
//	     获取参数
        var url = window.location.search;
//	     正则筛选地址栏
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
        // 匹配目标参数
        var result = url.substr(1).match(reg);
        //返回参数值
        return result ? decodeURIComponent(result[2]) : null;
    }

    //注销
    $scope.logout=function(){
        window.location.href = URL.baseurl + 　'login.html';
    }
})