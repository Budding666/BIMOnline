angular.module('myApp.carData')
    .component('maintainList2Component', {
        templateUrl: 'car_data_module/maintain_list2.html',
        css: 'car_data_module/maintain_list2.css',
        controller: function ($scope, $http, $state) {

            console.log("This is maintain_list2 component")

            //获取维修记录 暂时没记录
            $scope.maintainList = [];
            $scope.showMtList = function (pg) {
                var obj = {
                    page: pg,
                    rows: 6
                }
                $http.post(baseurl + "/list2/select/maintain", obj).then(
                    (res) => {
                        if (res.data.ret === 1) {
                            console.log(res.data.rows);
                            //处理请求到的图片
                            if (res.data.rows) {
                                res.data.rows.forEach(v => {
                                    // console.log(">>>>>" + v.img);
                                    v.img = JSON.parse(v.img);
                                    // console.log("<<<<<" + v.img);
                                    //显示审核结果
                                    if (v.statue === '2') {
                                        console.log('this statue is 2')
                                        v.examineResultRefused = '已拒绝支付';
                                        v.isPassedShow = false;
                                        v.isRefusedShow = true;
                                        v.isUnknowShow = false;
                                        // v.isBtnShow = false;
                                    } else if (v.statue === '1') {
                                        console.log('this statue is 1')
                                        v.examineResultPassed = '费用已支付';
                                        v.isPassedShow = true;
                                        v.isRefusedShow = false;
                                        v.isUnknowShow = false;                                        
                                        // v.isBtnShow = false;
                                    } else if (v.statue === '0') {
                                        v.examineResultUnknow = '等待审核支付';
                                        v.isPassedShow = false;
                                        v.isRefusedShow = false;
                                        v.isUnknowShow = true;                                        
                                        // v.isBtnShow = true;
                                    }
                                });
                                $scope.items = res.data.rows;
                            
                                if ($scope.items) {
                                    $scope.maintainList.push(...$scope.items);
                                }
                                if (res.data.rows < obj.rows) {
                                    $('.loadmore').html("没有更多了");
                                }
                                //关闭loading动画
                                $('.loading-img').css('display', 'none');
                                $('.loadmore').css('display', 'block');
                            } 
                        }else if (res.data.ret === -1) {
                            layer.alert("暂无维修记录..");
                        } else if (res.data.ret === -3) {
                            layer.alert("身份验证失败，请重新登录..");
                            window.location.href = URL.baseurl + 　'login.html';
                        }

                    },
                    (err) => {
                        console.log(err);
                        $('.loadmore').css('display', 'none');
                    }
                )
            }
            $scope.showMtList(1);
            //加载更多
            var page = 1;
            $scope.loadMore = function () {
                console.log("点击了loadMore")
                $('.loadmore').css('display', 'none');
                $('.loading-img').css('display', 'block');
                //从第2页开始加载
                page++;
                $scope.showMtList(page);
                return page;
            };
            //查看拒绝信息
            $scope.readMsg = function (info, id) {
                layer.alert(info, {
                    title: '拒绝支付信息',
                    icon: 5,
                    skin: 'layui-layer-lan'
                })
            }
            //点击图片时放大显示图片
            // $scope.enlargeImg = function ($event) {
            //     var img = $event.srcElement || $event.target;
            //     console.log(img.src);
            //     $("#bigimage").attr("src", img.src);
            //     console.log($("#bigimage")[0].src);
            //     // angular.element("#bigimage")[0].src = img.src;
            //     $("#js-imgview").css("display", "block");
            //     $("#js-imgview-mask").css("display", "block");
            //     // angular.element("#js-imgview-mask")[0].style.display = "block";
            // }
            // //点击图片时放小显示图片
            // $scope.closeImg = function () {
            //     $("#js-imgview").css("display", "none");
            //     $("#js-imgview-mask").css("display", "none");
            // }
            // 点击图片创建轮播图 可左右滑动 可两根手指控制缩放
            $(".box").on('click', function (e) {
                e.stopPropagation();
                $('#js-imgview-mask').css("display", "none");
            })
            $scope.createSwiper = function (imgs, index) {
                $("#js-imgview-mask").css("display", "block");
                console.log(imgs, index);
                $('.swiper-wrapper').html('');
                var ht = '';
                imgs.forEach(v => {
                    console.log(v);
                    ht += '<div class="swiper-slide"><div class="swiper-zoom-container"><img src=' + v + '></div></div>';
                });
                console.log(">>>>>>>>>", ht);
                $('.swiper-wrapper').html(ht);
                var mySwiper = new Swiper('.swiper-container', {
                    mode: 'horizontal', //水平轮播
                    // pagination: '.swiper-pagination',
                    initialSlide: index, //默认显示当前index索引位置的图片
                    loop: false,
                    zoom: {
                        maxRatio: 5, //最大缩放倍率
                        minRatio: 2, //最小缩放倍率
                        toggle: true //允许双击缩放
                    },
                    observer: true, //修改swiper自己或子元素时，自动初始化swiper
                    observeParents: true, //修改swiper的父元素时，自动初始化swiper
                    touchMoveStopPropagation: true, //阻止touchmove冒泡事件
                    // on:{
                    //     click: function(event){
                    //         // alert('你点了Swiper');
                    //         event.stopPropagation();
                    //         console.log(event.target);
                    //         // $("#js-imgview-mask").css("display", "none");
                    //     },//回调函数  slider点击事件
                    //     // tap:function(){
                    //     //     $("#js-imgview-mask").css("display", "none");
                    //     // }
                    // },
                });
                return mySwiper;
            }
        }
    })