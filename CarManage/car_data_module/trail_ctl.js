angular.module('myApp.carData')
.component('trailComponent',{
    templateUrl:'car_data_module/trail.html',
    css:'car_data_module/trail.css',
    controller:function($scope, $state, $stateParams){

        console.log("This is trail component")

        $('#date').jHsDate({
            callBack: function () {
                alert('确定搜索该时间段车辆轨迹？')
            }
        });
        $('#stratTime').jHsDate({
            format:'hh:mm',
            callBack: function () {
                alert('确定搜索该时间段车辆轨迹？')
            }
        });
        $('#endTime').jHsDate({
            format:'hh:mm',
            callBack: function () {
                alert('确定搜索该时间段车辆轨迹？')
            }
        });

        $(".choice-date").on("click", function (e) {
            e = window.event || e; // 兼容IE7
            obj = $(e.srcElement || e.target);
            if (!$(obj).is("[name='jHsDateInput']") && !$(obj).is('[class^="hs_"]')) {
                $('[name="jHsDate"]').remove();
            }
        })

        $carNum = $stateParams.carNum

        $scope.toTrailMap = function (carId) {
            console.log("trail发送参数-trail-map:", carId);
            $state.go('trailMap', {carId: carId});
        }
    }
})