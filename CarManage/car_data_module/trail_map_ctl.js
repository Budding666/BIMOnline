angular.module('myApp.carData')
    .component('trailMapComponent',{
        templateUrl:'car_data_module/trail_map.html',
        css:'car_data_module/trail_map.css',
        controller:'trailMapCtl'
    })
    .controller('trailMapCtl', function($scope, $location, $state, $stateParams,$http){

        /************************获取传递的参数*******************************/
        if (!localStorage.getItem('trail_car_id')) {
            localStorage.setItem('trail_car_id', $stateParams.car_id);
        }
        if (!localStorage.getItem('trail_car_num')) {
            localStorage.setItem('trail_car_num', $stateParams.car_num);
        }

        $scope.carId = localStorage.getItem('trail_car_id');
        $scope.carNum = localStorage.getItem('trail_car_num');
        console.log($scope.carId, $scope.carNum);
        /*********************************************************************/


        /****************************百度地图*********************************/
        //初始化地图,并设置中心点坐标
        $scope.track_map = new BMap.Map("container");
        $scope.track_map.centerAndZoom( new BMap.Point(116.404, 39.915), 10);

        //车辆的标志
        $scope.myIcon = new BMap.Icon('static/images/trail_map/origin_car.png', new BMap.Size(40, 23),{
            anchor: new BMap.Size(0, 0),
            imageOffset: new BMap.Size(0, 0)
        });
        $scope.marker = null; //用来放置坐标点的标记
        $scope.line = null; //用来放置线条覆盖物
        $scope.points = [];   //用来放置返回的所有坐标点
        /*********************************************************************/

        /***************************通用函数格式化日期**************************/
        $scope.getDate = function(){
            let date = new Date();
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let day = date.getDate();
            let dateStr  = "" + year + "-";
            if(month<10){
                dateStr += "0" + month + "-";
            }else{
                dateStr += month + "-";
            }

            if(day < 10){
                dateStr += "0" +day;
            }else{
                dateStr += day;
            }
            return dateStr
        };
        /*******************************************************************/


        /*************************默认时间或者选择时间************************/
        $scope.getTrail = function( queryStr ){

            let op = {
                car_id: $scope.carId,
                time_mysql: queryStr,
                type:2
            }
            console.log( op );
            console.log("This is asd");
            $http.post(baseurl + '/one/track_map2', op).then((res)=>{
                console.log(res)
                if( res.data.ret == 1){

                    let longtitude = JSON.parse(res.data.info.longitude_arr);
                    let lantitude = JSON.parse(res.data.info.latitude_arr);

                    //记录新坐标点的长度:
                    let length = longtitude.length;

                    //将每次从服务器获取的point遍历完成以后的坐标点放入 $scope.points
                    for(let i=0; i < length; i++){

                        let p0 = longtitude[i];
                        let p1 = lantitude[i];
                        let Point = new BMap.Point(p0, p1);
                        $scope.points.push(Point);

                    }

                    //删除原来的marker设置新的marker,并设置最新的marker 设置中心点坐标
                    if(!$scope.marker){ //首次获取的数据

                        $scope.marker = new BMap.Marker( $scope.points[$scope.points.length-1], {icon: $scope.myIcon} );
                        $scope.line = new BMap.Polyline($scope.points, {
                            strokeColor: "blue",
                            strokeWeight: 3,
                            strokeOpacity: 0.5
                        });

                        $scope.track_map.addOverlay($scope.marker);
                        $scope.track_map.addOverlay($scope.line);

                    }else{ //首次之外获取的数据

                        //清除点,清除线条覆盖物
                        $scope.track_map.removeOverlay($scope.marker);
                        $scope.track_map.removeOverlay($scope.line);

                        //设置新的marker
                        $scope.marker = new BMap.Marker( $scope.points[$scope.points.length-1], {icon: $scope.myIcon} );
                        //新增的点
                        let newAddPoints = $scope.points.slice(length);
                        $scope.line = new BMap.Polyline(newAddPoints, {
                            strokeColor: "blue",
                            strokeWeight: 3,
                            strokeOpacity: 0.5
                        });

                        $scope.track_map.addOverlay($scope.marker);
                        $scope.track_map.addOverlay($scope.line);

                    }

                    //每次都用最新点设置中心点
                    $scope.track_map.centerAndZoom( $scope.points[ $scope.points.length-1 ], 12);

                }
                else if(res.data.ret === -1) {
                    layer.msg(
                        "此时间段无车辆轨迹信息",
                        {
                            icon:5,
                            time:3000
                        },
                        function(index){
                            layer.close(index);
                        }
                    )
                }

            },(err)=>{
                layer.msg(
                    "服务器出错", {
                        icon: 5,
                        time: 3000
                    },
                    function (index) {
                        layer.close(index);
                    }
                )
            })
        };

        /**************************************************************/

        //$scope.getTrail();


        //如果需要每次都获取最新点,设置时间间隔 最后删除即可
        // $scope.$on("$destroy", function(){
        //     clearInterval(intervalNum)
        // })

        /***********************从插件获取数据,以及搜索时间************************/
        $scope.startDate = null;
        $scope.endDate = null;

        //layui日期选择
        layui.laydate.render({
            elem: '#date1',
            type: 'date', //默认，可不填
            theme: '#00722e',
            max: 0,
            // change: function (value) {
            //     console.log(value); //得到日期生成的值，如：2017-08-18
            //     console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            //     console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
            // },
            done: function (value) {
                console.log(value); //得到日期生成的值，如：2017-08-18
                $scope.startDate = value;
            }
        });




        $scope.goSearch = function(){
            console.log("click me");
            if( $scope.startDate == null ){
                layer.msg(
                    "请选择日期",
                    {
                        icon:5,
                        time:3000
                    },
                    function(index){
                        layer.close(index);
                    }
                )
            }else{
                $scope.endDate = $scope.startDate + " 23:59:59";
                $scope.startDate += " 00:00:00";
                let queryStr = $scope.startDate + "," + $scope.endDate;
                console.log(queryStr);
                $scope.getTrail( queryStr );
            }
        }
        /************************************************************************/
    });