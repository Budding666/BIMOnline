angular.module('myApp.carData',[
    'ui.router',
    'angularCSS',
    'myApp.server'
])
.config(function($stateProvider){

    $stateProvider.state('real_data', {
        url:'/real_data',
        component:'realDataComponent',
    });

    $stateProvider.state('trail',{
        url:'/trail/:carId',
        component:'trailComponent',
    });

    $stateProvider.state('char_analysis', {
        url:'/char_analysis',
        component:'charAnalysis',
    });

    $stateProvider.state('maintain_list', {
        url:'/maintain_list',
        component:'maintainListComponent',
    });

    $stateProvider.state('maintain_list2', {
        url: '/maintain_list2',
        component: 'maintainList2Component',
    });

    $stateProvider.state('trail_map', {
        params: {
            car_id: null,
            car_num:null
        },
        url:'/trail_map/',
        component:'trailMapComponent',
    });
});

