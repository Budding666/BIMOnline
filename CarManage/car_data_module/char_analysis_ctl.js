angular.module('myApp.carData')
.component("charAnalysis",{
    templateUrl:'car_data_module/char_analysis.html',
    css:'car_data_module/char_analysis.css',
    controller:'chartAnaCtl'
})
.controller('chartAnaCtl',function($scope, $http){
    $scope.driver_info;
/***********************************************************************************/
    // $scope.carInfo = ["A12345", "B12345", "C789456", "D98643"];
    // $scope.currentCar = $scope.carInfo[1];
    //
    // $scope.select_car;
    // $scope.have_selected = false;
    // $scope.li_click = function(e){
    //     $scope.select_car = e.target.innerHTML;
    //     // if($scope.select_car!=null){
    //     //     $scope.currentCar = $scope.select_car;
    //     //     $scope.select_car = null;
    //     //     $scope.have_selected = false
    //     // }
    //     console.log($scope.select_car)
    //     $scope.have_selected = false
    //     $scope.currentCar = $scope.select_car;
    // }
    //
    // $(document).on('click', function(e){
    //     if(e.target.id == 'show_div'){
    //         $scope.have_selected = !$scope.have_selected;
    //     }
    //     else if(e.screenY < window.innerHeight){
    //         $scope.have_selected = false;
    //     }
    //     $scope.$apply()
    // })
    //
    //
    //
    // //li标签样式设置
    // $('.header_li:not(:first)').on('click',()=>{
    //     $('.first').css('border-bottom','none');
    // })
    // $('.first').on('click',()=>{
    //     $('.first').css('border-bottom', '2px solid #fff');
    // })
    //
    // //获取数据
    // $scope.data = [
    //     {
    //         title:'油耗',
    //         id:'container1',
    //         type:'bar',
    //         dataname:[],
    //         datascore:[],
    //     },
    //     {
    //         title:'行驶里程',
    //         id:'container2',
    //         type:'line',
    //         dataname:[],
    //         datascore:[],
    //     },
    //     {
    //         title:'数据分析',
    //         id:'container3',
    //         type:'pie',
    //         dataname:[],
    //         datascore:[],
    //     }
    // ]
    // $scope.VsData = [
    //
    // ]
    // function set_vsData(id){
    //     var chart = echarts.init(document.getElementById(id));
    //     var option = {
    //         grid:{
    //             top:40,
    //             left:40,
    //         },
    //         xAxis: {
    //             data: ["A", "B", "C", "D"],
    //             axisLine:{
    //                 lineStyle:{
    //                     color:'#fff',
    //                     width:1,//这里是为了突出显示加上的
    //                 }
    //             },
    //             axisLabel:{
    //                 interval:0,
    //                 fontSize:8
    //             }
    //         },
    //         yAxis: {
    //             axisLine:{
    //                 lineStyle:{
    //                     color:'#fff',
    //                     width:1,//这里是为了突出显示加上的
    //                 }
    //             }
    //         },
    //         series: [{
    //             type:"line",
    //             data:[10, 20, 40, 30]
    //         },{
    //             type:"line",
    //             data:[20, 10, 30, 50]
    //         }]
    //     };
    //     chart.setOption(option)
    // }
    // set_vsData("vs1")
    // set_vsData("vs2")
    /***********************************************************************************/
    var search = new Date(Date.now())
    console.log(search)
    var year = search.getFullYear()
    var month = search.getMonth() + 1
    var day = search.getDate()
    var hours = search.getHours()
    var minutes = search.getMinutes()
    var seconds = search.getSeconds()
    console.log(day)
    var start = year + "-" + month + "-" + day + " 00:00:00";
    var end = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
    console.log(start)
    console.log(end)
    $http.post(baseurl+"/all_driver_info?car_role==2",{time:start,endtime:end}).then((res)=>{
        if(res.data.ret === 1){
            $scope.driver_info = res.data.info
            console.log($scope.driver_info)
        }else{
            layer.msg(
                "没有找到！", {
                    icon: 5,
                    time: 2000
                },
                function (index) {
                    layer.close(index);
                }
            )
        }
    },(err)=>{
        layer.msg(
            "服务器出错", {
                icon: 5,
                time: 2000
            },
            function (index) {
                layer.close(index);
            }
        )
    });
    /***********************************************************************************/
    // function set_data(data, interval){
    //     for(var i=0; i< data.length; i++){
    //         // console.log(data[i].id)
    //         var chart = echarts.init(document.getElementById(data[i].id));
    //         //这里依据data给的不同的值设置选项,如果是line则设置linde 的选项
    //         //如果是pie则设置pie的选项
    //         var option = {
    //             grid:{
    //                 top:40,
    //                 left:40,
    //             },
    //             xAxis: {
    //                 data: [],
    //                 axisLine:{
    //                     lineStyle:{
    //                         color:'#fff',
    //                         width:1,//这里是为了突出显示加上的
    //                     }
    //                 },
    //                 axisLabel:{
    //                     interval:0,
    //                     fontSize:8
    //                 }
    //             },
    //             yAxis: {
    //                 axisLine:{
    //                     lineStyle:{
    //                         color:'#fff',
    //                         width:1,//这里是为了突出显示加上的
    //                     }
    //                 }
    //             },
    //             series: []
    //         };
    //         option.series.push({
    //             type:data[i].type,
    //             data:data[i].datascore
    //         })
    //
    //         //这里进行选择判断,给予不同的设置
    //         if(data[i].type == 'pie'){
    //             option.xAxis = null;
    //             option.yAxis = null;
    //         }else{//柱状图
    //             option.xAxis.data = data[i].dataname;
    //             option.xAxis.axisLabel.interval = interval
    //         }
    //
    //         chart.setOption(option)
    //     }
    // }
    //
    // $scope.aDay = function(){
    //     for(var i=0; i<$scope.data.length; i++){
    //         //将数据清空
    //         $scope.data[i].dataname = [];
    //         $scope.data[i].datascore = [];
    //         //判断是否获取到了数据,如果获取到，则将数据放入数组
    //         $scope.data[i].dataname = ["6:00-9:00", "9:00-12:00", "1:00-4:00", "4:00-6:00"]
    //         $scope.data[i].datascore = [10, 20, 40, 30]
    //     }
    //     setTimeout(() => {
    //          set_data($scope.data, 0);
    //     }, 0);
    // }
    // $scope.aDay();
    //
    //
    //
    // $scope.aWeek = function(){
    //     for(var i=0; i<$scope.data.length; i++){
    //         $scope.data[i].dataname = [];
    //         $scope.data[i].datascore = [];
    //
    //         $scope.data[i].dataname = ["1", "2", "3", "4", "5", "6", "7"]
    //         $scope.data[i].datascore = [10, 20, 40, 30, 25, 70, 30]
    //     }
    //     set_data($scope.data, 0);
    // }
    //
    // $scope.aMonth = function(){
    //     for(var i=0; i<$scope.data.length; i++){
    //         $scope.data[i].dataname = [];
    //         $scope.data[i].datascore = [];
    //
    //         $scope.data[i].dataname = ["1", "2", "3", "4", "5", "6", "7",
    //             "8", "9", "10", "11", "12", "13", "14",
    //             "15", "16", "17", "18", "19", "20", "21",
    //             "22", "23", "24", "25", "26", "27", "28",
    //             "29", "30", "31", "32"]
    //         $scope.data[i].datascore = [10, 20, 40, 30, 25, 70, 30,
    //             10, 20, 40, 30, 25, 70, 30,
    //             10, 20, 40, 30, 25, 70, 30,
    //             10, 20, 40, 30, 25, 70, 30,
    //             10, 20, 40, 20]
    //     }
    //     set_data($scope.data, 0);
    //     //获取一个月的数据
    // }
    // $scope.aYear = function(){
    //     for(var i=0; i<$scope.data.length; i++){
    //         $scope.data[i].dataname = ["1", "2", "3", "4", "5", "6", "7",
    //             "8", "9", "10", "11", "12"]
    //         $scope.data[i].datascore = [10, 20, 40, 30, 25, 70, 30,
    //             10, 20, 40, 30, 25]
    //     }
    //     set_data($scope.data, 0);
    // }
    /***********************************************************************************/
})