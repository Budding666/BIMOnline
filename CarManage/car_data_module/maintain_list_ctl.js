angular.module('myApp.carData')
.component('maintainListComponent',{
    templateUrl:'car_data_module/maintain_list.html',
    css:'car_data_module/maintain_list.css',
    controller:function($scope, $http,$state){

        console.log("This is maintain_list component")
        $scope.msgReg = /[@#\$%\^&\*]+/g
        //maintian_list页面搜索功能
        $scope.reg = /([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领]{1}[A-Z]{1}(([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1})/;
        $scope.searchMaintain = function () {
            $scope.infos={};
            var carNum = $('#searchMaintain').val().toUpperCase();
            // console.log(carNum);
            if(carNum===null||carNum===''){
                layer.msg(
                    "车牌号不能为空",
                    {
                        icon:7,
                        time: 2000
                    }
                );
                return;
            }else if(!$scope.reg.test(carNum)){
                layer.msg(
                    "您输入的车牌号码格式不正确", {
                        icon: 7,
                        time: 2000
                    }
                );
                return;
            }else{
                console.log(carNum);
                $http.post(baseurl + "/Maintenance/records/maintain", {
                    car_num: carNum
                }).then(
                    (res)=>{
                        $('.loadmore').css('display', 'none');
                        $('.repair-box').css('display', 'none');
                        $('.searchResult').css('display', 'block');
                        console.log(res);
                        if(res.data.rows.length==0){
                            layer.msg(
                                "没有该车辆的维修记录", {
                                    icon: 5,
                                    time: 2000
                                }
                            )
                        }
                        if(res.data.ret===1){
                            if (res.data.rows) {
                                res.data.rows.forEach(v => {
                                    // console.log(">>>>>" + v.img);
                                    v.img = JSON.parse(v.img);
                                    // console.log("<<<<<" + v.img);
                                    //判断显示审核操作按钮or审核结果
                                    if (v.statue === '2') {
                                        console.log('this statue is 2')
                                        v.examineResultRefused = '您已拒绝';
                                        v.isPassedShow = false;
                                        v.isRefusedShow = true;
                                        v.isBtnShow = false;
                                    } else if (v.statue === '1') {
                                        console.log('this statue is 1')
                                        v.examineResultPassed = '您已支付';
                                        v.isPassedShow = true;
                                        v.isRefusedShow = false;
                                        v.isBtnShow = false;
                                    } else if (v.statue === '0') {
                                        v.isPassedShow = false;
                                        v.isRefusedShow = false;
                                        v.isBtnShow = true;
                                    }
                                });
                                $scope.infos = res.data.rows;
                            }
                        }else if(res.data.ret===-1){
                            layer.msg(
                                "没有该车辆的维修记录",
                                {
                                    icon:5,
                                    time:2000
                                }
                            )
                        }else if(res.data.ret===-3){
                            layer.msg(
                                "身份验证失败，请重新登录..", {
                                    icon: 4,
                                    time: 2000
                                }
                            )
                        }
                    },
                    (err)=>{
                        console.log(err);
                        layer.msg(
                            "服务器维护中，请稍候再试..", {
                                icon: 5,
                                time: 2000
                            }
                        )
                    }
                )
            };
        };
        //搜索页面返回按钮功能
        $scope.returnBtn = function () {
            console.log('返回');
            $('.searchResult').css('display', 'none');
            $('.repair-box').css('display', 'block');
            $('.loadmore').css('display', 'block');
        }

        //获取维修记录 暂时没记录
        $scope.maintainList=[];
        $scope.showMtList=function(pg){
            var obj={page:pg,rows:6}
            console.log(baseurl + "/list2/select/maintain")
            $http.post(baseurl + "/list2/select/maintain", obj).then(
                (res)=>{
                    if(res.data.ret===1){
                        console.log(res.data.rows);
                        //处理请求到的图片
                        if (res.data.rows) {
                            res.data.rows.forEach(v => {
                                // console.log(">>>>>" + v.img);
                                v.img = JSON.parse(v.img);
                                // console.log("<<<<<" + v.img);
                                //判断显示审核操作按钮or审核结果
                                if (v.statue === '2') {
                                    console.log('this statue is 2')
                                    v.examineResultRefused = '您已拒绝';
                                    v.isPassedShow = false;
                                    v.isRefusedShow = true;
                                    v.isBtnShow = false;
                                } else if (v.statue === '1') {
                                    console.log('this statue is 1')
                                    v.examineResultPassed = '您已支付';
                                    v.isPassedShow = true;
                                    v.isRefusedShow = false;
                                    v.isBtnShow = false;
                                } else if (v.statue === '0') {
                                    v.isPassedShow = false;
                                    v.isRefusedShow = false;
                                    v.isBtnShow = true;
                                }
                            });
                            $scope.items = res.data.rows;
                        }
                        if ($scope.items) {
                            $scope.maintainList.push(...$scope.items);
                        }
                        if (res.data.rows < obj.rows) {
                            $('.loadmore').html("没有更多了");
                        }
                        //关闭loading动画
                        $('.loading-img').css('display', 'none');
                        $('.loadmore').css('display', 'block');
                    }else if(res.data.ret===-1){
                        layer.alert("暂无维修记录..");
                    }else if(res.data.ret===-3){
                        console.log(res.data)
                        layer.alert("身份验证失败，请重新登录..");
                        window.location.href = URL.baseurl + 　'login.html';
                    }
                
                },
                (err)=>{
                    console.log(err);
                    $('.loadmore').css('display', 'none');                  
                }
            )
        }
        $scope.showMtList(1);
        //处理审核功能
        $scope.setStatue = {};
        $scope.pass = function (item) {
            // item.isBtnShow = false;
            layer.confirm('请确定您已经为司机支付了该维修费用', {
                btn: ['确定', '取消'], //按钮
                yes: function () {
                    $scope.setStatue.statue = '1';
                    $http.post(baseurl + '/edit_maintain_statue?id==' + item.id, $scope.setStatue).then(
                        (res) => {
                            console.log(res);
                            if (res.data.ret === 1) {
                                layer.msg(
                                    "您的支付信息已提交", {
                                        icon: 1,
                                        time: 2000,
                                    },
                                    function (index) {
                                        $state.reload();
                                        layer.close(index);
                                    }
                                );
                            } else if (res.data.ret === -3) {
                                layer.msg(
                                    "身份验证失败，请重新登录", {
                                        icon: 4,
                                        time: 2000,
                                    },
                                    function (index) {
                                        window.location.href = URL.baseurl + 　'login.html';
                                        layer.close(index);
                                    }
                                );
                            }
                        },
                        (err) => {
                            console.log(err)
                            layer.msg(
                                "服务器繁忙，请稍候再试..", {
                                    icon: 4,
                                    time: 2000,
                                },
                                function (index) {
                                    layer.close(index);
                                }
                            );
                            // item.isBtnShow = true;
                        }
                    );
                },
                btn2: function (index) {
                    // item.isBtnShow = true;
                    layer.close(index);
                }
            })
        }
        $scope.refuse = function (item) {
            // item.isBtnShow = false;
            var index = layer.prompt({
                    title: ['请填写您拒绝支付维修费用的理由！(必填)', 'font-size:14px,color:#333'],
                    formType: 2,
                    area: ['300px', '100px'],
                    maxlength: 140, //可输入文本的最大长度，默认500
                    value: '',
                },
                function (value, index) {
                    //替换掉用户输入的特殊字符
                    var rs='';
                    for (let i = 0; i < value.length; i++) {
                        rs = rs + value.substr(i, 1).replace($scope.msgReg, '');
                    }
                    console.log(rs);
                    $scope.setStatue.statue = '2';
                    $scope.setStatue.info = rs;
                    $http.put(baseurl + '/edit_car/maintain?id=' + item.id + '', $scope.setStatue).then(
                        (res) => {
                            if (res.data.ret === 1) {
                                layer.msg(
                                    "您拒绝支付维修费用的信息已提交！", {
                                        icon: 1,
                                        time: 2000,
                                    },
                                    function (index) {
                                        $state.reload();
                                        layer.close(index);
                                    }
                                );
                            } else if (res.data.ret === -3) {
                                layer.msg(
                                    "身份验证失败，请重新登录", {
                                        icon: 4,
                                        time: 2000,
                                    },
                                    function (index) {
                                        window.location.href = URL.baseurl + 　'login.html';
                                        layer.close(index);
                                    }
                                );
                            }
                        },
                        (err) => {
                            layer.msg(
                                "服务器繁忙，请稍候再试..", {
                                    icon: 4,
                                    time: 2000,
                                },
                                function (index) {
                                    layer.close(index);
                                }
                            );
                            // item.isBtnShow = true;
                        }
                    )
                    layer.close(index)
                }
            )
        }

        //加载更多
        var page = 1;
        $scope.loadMore = function () {
            console.log("点击了loadMore")
            $('.loadmore').css('display', 'none');
            $('.loading-img').css('display', 'block');
            //从第2页开始加载
            page++;
            $scope.showMtList(page);
            return page;
        };
         //查看拒绝信息
         $scope.readMsg = function (info) {
             if (info === null) {
                 info = "以前车主没有留下拒绝支付信息！"
             }
             layer.alert(info, {
                 title: '您的拒绝支付信息',
                 icon: 5,
                 skin: 'layui-layer-lan'
             })
         }
        //点击图片时放大显示图片
        // $scope.enlargeImg = function ($event) {
        //     var img = $event.srcElement || $event.target;
        //     console.log(img.src);
        //     $("#bigimage").attr("src", img.src);
        //     console.log($("#bigimage")[0].src);
        //     // angular.element("#bigimage")[0].src = img.src;
        //     $("#js-imgview").css("display","block");
        //     $("#js-imgview-mask").css("display", "block");
        //     // angular.element("#js-imgview-mask")[0].style.display = "block";
        // }
        //点击图片时放小显示图片
        // $scope.closeImg = function () {
        //     $("#js-imgview").css("display", "none");
        //     $("#js-imgview-mask").css("display", "none");
        // }
        // 点击图片创建轮播图 可左右滑动 可两根手指控制缩放
        $(".box").on('click', function (e) {
            e.stopPropagation();
            $('#js-imgview-mask').css("display", "none");
        })
        $scope.createSwiper = function (imgs, index) {
            $("#js-imgview-mask").css("display", "block");
            console.log(imgs, index);
            $('.swiper-wrapper').html('');
            var ht = '';
            imgs.forEach(v => {
                console.log(v);
                ht += '<div class="swiper-slide"><div class="swiper-zoom-container"><img src=' + v + '></div></div>';
            });
            console.log(">>>>>>>>>", ht);
            $('.swiper-wrapper').html(ht);
            var mySwiper = new Swiper('.swiper-container', {
                mode: 'horizontal', //水平轮播
                // pagination: '.swiper-pagination',
                initialSlide: index, //默认显示当前index索引位置的图片
                loop: false,
                zoom: {
                    maxRatio: 5, //最大缩放倍率
                    minRatio: 2, //最小缩放倍率
                    toggle: true //允许双击缩放
                },
                observer: true, //修改swiper自己或子元素时，自动初始化swiper
                observeParents: true, //修改swiper的父元素时，自动初始化swiper
                touchMoveStopPropagation: true, //阻止touchmove冒泡事件
                // on:{
                //     click: function(event){
                //         // alert('你点了Swiper');
                //         event.stopPropagation();
                //         console.log(event.target);
                //         // $("#js-imgview-mask").css("display", "none");
                //     },//回调函数  slider点击事件
                //     // tap:function(){
                //     //     $("#js-imgview-mask").css("display", "none");
                //     // }
                // },
            });
            return mySwiper;
        }
    }
})