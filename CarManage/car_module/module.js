angular.module('myApp.car', [
    'ui.router',
    'angularCSS'
])
.config(function($stateProvider){

    $stateProvider.state('addCar',{
        url:'/add_car',
        component:'addCarComponent',
    });
    $stateProvider.state('carList', {
        params: {
            car_num: null
        },
        url: '/car_list',
        component: 'carListComponent',
    });
    $stateProvider.state('carList2', {
        params:{car_num:null},
        url: '/car_list2',
        component: 'carList2Component',
    });
    $stateProvider.state('addMaintain',{
        params:{car_num:null},
        url:'/addMaintain',
        component:'addMaintainComponent',
    });
    $stateProvider.state('edit_car_info', {
        params:{
            id:null,
            car_num:null,
            cartype:null,
            create_time:null,
            rent_money:null,
            terminal_num:null
        },
        url: '/edit_car_info',
        component: 'editCarInfoComponent',
    });
})
// .service('carService', function ($http, $cookieStore){
//     this.showService= function(){
//         console.log("This is myService");
//     }
//     //分页查询车辆信息
//     this.showList=function(pg){
//         console.log("this is showList")
//         var obj={page:pg,rows:8};
        
//         var data=$http.post(baseurl + "/list2/select/carinfo", obj).then(function (res) {
//             console.log(res.data.rows);

//             // if (res.data.rows) {
//             //     console.log(res.data.rows);
//             //     if (res.data.rows.length !== 0 && res.data.rows !== undefined) {
//             //        console.log(res.data.rows);
//             //     }
//             // } 
//         // //     else {
//         // //         page--;
//         // //     }
//         });
//     }
//     //加载更多
//     var page = 1;
//     this.loadMore=function(){
//         page++;
//         showList(page);
//         return page;
//     }
// })
// .service('CarServer',function($scope,$http){
    //分页查询信息 上划加载
    // this.getData = function(){
    //     console.log("getCarListServer")
    // }
//     var page=1;
//    this.loadMore=function () {
//         page++;
//         this.showList(page);
//         return page;
//     }
//     this.showList=function(pg){
//         var obj={page:pg,rows:8};
//         var data=$http.post(baseurl + "/list2/select/carinfo", obj).then(function (res) {
//             if (res.data.rows) {
//                 console.log(res.data.rows);
//                 if (res.data.rows.length !== 0 && res.data.rows !== undefined) {
//                     return res.data.rows;
//                 }
//             } else {
//                 page--;
//             }
//         })
//         console.log(data);
//     }
// })