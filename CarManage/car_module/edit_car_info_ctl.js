angular.module('myApp.car')
.component('editCarInfoComponent', {
    templateUrl: 'car_module/edit_car_info.html',
    css: 'car_module/edit_car_info.css',
    controller: function ($scope, $http, $stateParams, $state, $cookieStore) {
        $scope.subDisable = true;        
        //定义要缓存的对象接受传过来的参数
        var carInfo_op = {
            id : $stateParams.id,
            car_num : $stateParams.car_num,
            create_time : $stateParams.create_time,
            rent_money : $stateParams.rent_money,
            terminal_num : $stateParams.terminal_num,
            cartype : $stateParams.cartype,
        };
        //缓存开始
        if (!localStorage.getItem('edit_carInfo_op')) {
            localStorage.setItem('edit_carInfo_op', JSON.stringify(carInfo_op));
            console.log("缓存传过来的参数" + JSON.parse(localStorage.getItem('edit_carInfo_op')))
        }

        console.log("缓存的参数" + JSON.parse(localStorage.getItem('edit_carInfo_op')));
        //获取预编辑车辆信息绑定到input框
        var localData = JSON.parse(localStorage.getItem('edit_carInfo_op'))
        $scope.id = localData.id;
        $scope.op={
            car_num: localData.car_num,
            cartype: localData.cartype,
            create_time: localData.create_time,
            rent_money: localData.rent_money,
            terminal_num: localData.terminal_num
        }
        //转换车型对应的数字编号
        switch (localData.cartype) {
            case '轿车':
                $scope.op.cartype = '1';
                break;
            case '运输车':
                $scope.op.cartype = '2';
                break;
            case '挖掘机':
                $scope.op.cartype = '3';
                break;
            case '渣土车':
                $scope.op.cartype = '4';
                break;
            case '其他车':
                $scope.op.cartype = '5';
                break;
        }
        //div绑定传过来的车型
        $('.select-type').html(localData.cartype);
        //设定车类型
        $scope.car_type = [{
                value: '1',
                text: '轿车'
            },
            {
                value: '2',
                text: '运输车'
            },
            {
                value: '3',
                text: '挖掘机'
            },
            {
                value: '4',
                text: '渣土车'
            },
            {
                value: '0',
                text: '其他车'
            }
        ];
        $scope.typeOpShow = false;
        $scope.isTypeShow = function () {
            if (!$scope.typeOpShow) {
                $scope.typeOpShow = !$scope.typeOpShow;
            } else {
                $scope.typeOpShow = !$scope.typeOpShow;
            }
        }
        $scope.choseType = function (item) {
            $('.select-type').html(item.text);
            // $scope.cartypeText = item.text;
            $scope.op.cartype = item.value;
            console.log($scope.op.cartype)
            $scope.typeOpShow = !$scope.typeOpShow;
        }

        console.log($scope.op.car_num);
        //input 车牌号显示不正确bug处理
        $scope.num_head = $scope.op.car_num[0]; 
        $scope.num_body = $scope.op.car_num.slice(1); // 传过来的省份
        //用户选择出厂日期
        // $('#carDatestart').jHsDate({
        //     callBack: function () {
        //         $scope.create_time = $('#carDatestart').val();
        //     }
        // });
        //layui日期选择
        layui.laydate.render({
            elem: '#carDatestart',
            type: 'date', //默认，可不填
            theme: '#00722e',
            max: 0,
            // change: function (value) {
            //     console.log(value); //得到日期生成的值，如：2017-08-18
            //     console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            //     console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
            // },
            done: function (value) {
                console.log(value); //得到日期生成的值，如：2017-08-18
                $scope.op.create_time = value;
            }
        });
        //用户选择车牌所属省份
        
        $scope.province = false; // 车牌选择框隐藏
        $scope.selectN = function () { // 隐藏显示车牌选择框
                if ($scope.province == false) {
                    $scope.province = true;
                } else {
                    $scope.province = false;
                }
            }
        $scope.numselect = function (num) { // 选择车牌
            $scope.num_head = num;
            $scope.province = false;
        };
        $scope.op.car_num = $scope.num_head + $scope.num_body;
        $scope.privence = ["京", "津", "沪", "渝", "蒙", "新", "藏", "宁", "桂", "港", "澳", "黑", "吉", "辽", "晋", "冀", "青", "鲁", "豫", "苏", "皖", "浙", "闽", "赣", "湘", "鄂", "粤", "琼", "甘", "陕", "贵", "云", "川"]
        
        //确认修改
        $scope.reg = /^[A-Za-z]{1}[A-Za-z0-9]{4}[A-Za-z0-9挂学警港澳]{1}$/;
        //再次检测是否为纯数字
        $scope.carNumTest = /^[a-zA-Z]{6}$/
        $scope.costs = /^[1-9][0-9]{1,7}$/;
        $scope.tNum = /^[a-zA-Z0-9]{8,15}$/;
         $scope.addCar = function () {
             $scope.op.create_time = $(".datestart").val();
             if ($scope.op.cartype == null || $scope.op.cartype === '') {
                layer.msg("车型号不能为空", {
                    icon: 5,
                    time: 2000, 
                });
                return;
             }
             if ($scope.num_body == null || $scope.num_body === '') {
                layer.msg("车牌号不能为空", {
                    icon: 5,
                    time: 2000,
                });
                return;
             } else if (!$scope.reg.test($scope.num_body)) {
                 layer.msg(
                     "请填写的车牌号格式不正确",
                     {
                        icon: 5,
                        time: 2000,
                     }    
                    )
                return;
             } else if ($scope.carNumTest.test($scope.num_body)) {
                 layer.msg(
                     "车牌号不能为纯字母", {
                         icon: 5,
                         time: 2000
                     }
                 )
                 return;
             } else if ($scope.op.create_time == null || $scope.op.create_time === '') {
                layer.msg("车辆生产日期不能为空", {
                    icon: 5,
                    time: 2000, 
                });
                return;
             } else if ($scope.op.terminal_num == null || $scope.op.terminal_num === '') {
                layer.msg("终端号不能为空", {
                    icon: 5,
                    time: 2000,
                });
                return;
             } else if (!$scope.tNum.test($scope.op.terminal_num)) {
                layer.msg("您填写的终端号格式不正确", {
                    icon: 5,
                    time: 2000,
                });
                return;
             }else if ($scope.op.rent_money == null || $scope.op.rent_money === '') {
                layer.msg("服务价格不能为空", {
                    icon: 5,
                    time: 2000,
                });
                return;
             } else if (!$scope.costs.test($scope.op.rent_money)) {
                layer.msg("您填写的价格格式不正确", {
                    icon: 5,
                    time: 2000,
                });
                return;
             }else  {
                $scope.subDisable = false;
                 
                $scope.op.car_num = $scope.num_head + $scope.num_body.toUpperCase();
                $http.put(baseurl + '/edit_car/carinfo?id=' + $scope.id, $scope.op).then(
                    (res)=>{
                        console.log(res);
                        console.log($scope.op);
                        if(res.data.ret===1){
                            layer.msg(
                                "编辑车辆信息成功！",
                                {
                                    icon:1,
                                    time:2000
                                },
                                function(index){
                                    $state.go('carList');
                                    $scope.subDisable = true;
                                    layer.close(index);
                                }
                            )
                        } else if (res.data.ret === -4) {
                            layer.msg("编辑车辆信息失败,该车已存在!", {
                                icon: 2,
                                time: 2000,
                            });
                        } else if (res.data.ret === -5) {
                            layer.msg("编辑车辆信息失败,该终端号已存在!", {
                                icon: 2,
                                time: 2000,
                            });
                        } else if (res.data.ret === -1) {
                            layer.msg(
                                "编辑车辆信息失败，车牌号后终端号已存在..",
                                {
                                    icon:2,
                                    time:2000
                                });
                        }else if(res.data.ret===-3){
                            layer.msg(
                                "用户身份验证失败，请重新登录..", {
                                    icon: 4,
                                    time:2000
                                },
                                function (index) {
                                    window.location.href = URL.baseurl + 　'login.html';
                                    layer.close(index);
                                }
                            );
                        }
                        $scope.subDisable = true;
                    },
                    (err)=>{
                        layer.msg(
                            "服务器忙，请稍候重试..",
                            {
                                icon:5,
                                time:2000
                            }
                        )
                        console.log(err);
                        $scope.subDisable = true;
                });
            }
         };
}})
