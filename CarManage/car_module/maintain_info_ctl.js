angular.module('myApp.car')
.component('addMaintainComponent',{
    templateUrl:'car_module/maintain_info.html',
    css:'car_module/maintain_info.css',
    controller: function ($scope, $http, $stateParams, $state) {
        $scope.subDisable = true;
        //返回按钮正确跳转
        //获取登录用户身份
        $scope.car_role = localStorage.getItem('car_role');
        $scope.fid = localStorage.getItem('fid');
        console.log($scope.car_role, $scope.fid);
        
        $scope.goBack=function(){
            if ($scope.fid == 1) {
                $state.go("home");
            } else
            if ($scope.car_role == '2') {
                $state.go("home2");
            } else {
                window.location.href = URL.baseurl + 　'login.html';
            }
        }


        $scope.op = {};
        $scope.op.img = [];
        /*时间插件*/
        // $('#repairDate').jHsDate({
        //     callBack:function(){
        //         $scope.op.time = $('#repairDate').val();
        //             //获取用户选择的时间
        //             $('#repairDate').attr('required','required');
                
        //     }
        // });
        //日期选择器
        layui.laydate.render({
            elem: '#repairDate',
            type: 'date', //默认，可不填
            theme: '#00722e',
            max: 0,
            // change: function (value) {
            //     console.log(value); //得到日期生成的值，如：2017-08-18
            //     console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            //     console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
            // },
            done: function (value) {
                console.log(value); //得到日期生成的值，如：2017-08-18
                $scope.op.time = value;
            }
        });
        document.documentElement.style.fontSize = document.documentElement.clientWidth * 0.1 + 'px';
        //缓存上页传过来的参数 防止页面刷新参数为null
        // if (!$cookieStore.get('maintain_car_num')) {
        //     $cookieStore.put('maintain_car_num', $stateParams.car_num);
        //     console.log("缓存的car_num：" + $cookieStore.get('maintain_car_num'));
        // }
        if (!localStorage.getItem('maintain_car_num')) {
            localStorage.setItem('maintain_car_num', $stateParams.car_num);
            console.log("缓存的car_num：" + localStorage.getItem('maintain_car_num'));
        }
        /*添加维修记录开始*/
        console.log("刷新后的car_num：" + localStorage.getItem('maintain_car_num'));

        $scope.op.car_num = localStorage.getItem('maintain_car_num');

        $scope.upload=tinyImgUpload('#upload');

        $scope.address = /^[\u4E00-\u9FA5][\d\u4E00-\u9FA5]+/;
        $scope.pers = /^[\u4e00-\u9fa5]{2,7}$/;
        $scope.costs = /^[1-9][0-9]{1,7}$/;
        $scope.lastRepair = function () {
            console.log($scope.op.img);
            console.log("this is time:", $scope.op.time)
            if ($scope.op.time === undefined || $scope.op.time === '') {
                layer.msg("请选择维修时间", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if ($scope.op.position === null || $scope.op.position === '' || $scope.op.position === undefined) {
                layer.msg("请填写维修地点", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if (!$scope.address.test($scope.op.position)) {
                layer.msg("您填写的维修地点格式不正确", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if ($scope.op.money === null || $scope.op.money === '' || $scope.op.money === undefined) {
                layer.msg("请填写维修金额", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if (!$scope.costs.test($scope.op.money)) {
                layer.msg("您填写的维修金额格式不正确", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if ($scope.op.personnel === null || $scope.op.personnel === '' || $scope.op.personnel===undefined) {
                layer.msg("请填写维修人员", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if (!$scope.pers.test($scope.op.personnel)) {
                layer.msg("您填写的维修人员姓名格式不正确", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else {
                $scope.subDisable = false;
                var myFormData=$scope.upload();
                //图片数组转存
                $.ajax({
                    url: "http://sxzd365.com:9060/photos/upload2",
                    type: "POST",
                    async: true,
                    data: myFormData,
                    processData: false,
                    contentType: false,
                    success:(res)=>{
                        console.log(res.result);
                        $scope.op.img = JSON.stringify(res.result);
                        $scope.op.statue = '0';
                        //添加维修记录开始
                        $http.post(baseurl + '/add_car/maintain', $scope.op).then(
                            (res)=>{
                                console.log(res);
                                if (res.data.ret === 1) {
                                    layer.msg(
                                        "添加维修记录成功！",
                                        {
                                            icon:1,
                                            time:1500
                                        },
                                        function(index){
                                            $scope.subDisable = true;
                                            layer.close(index);
                                        }
                                    )
                                    $state.reload();
                                } else if (res.data.ret !== -1) {
                                    layer.confirm('添加失败，请稍候重试..', {
                                        icon: 7,
                                        title: '提示',
                                        btn:["确定"]
                                    }, function (index) {
                                        //do something  
                                        layer.close(index);
                                    });
                                }else if(res.data.ret===-3){
                                    layer.msg(
                                        "身份验证失败，请重新登录！", {
                                            icon: 4,
                                            title: "提示",
                                            time: 2000,
                                        },
                                        function (index) {
                                            window.location.href = URL.baseurl + 　'login.html';
                                            layer.close(index);
                                        }
                                    )
                                }
                                $scope.subDisable = true;
                            },
                            (err)=>{
                                console.log(err);
                                layer.msg(
                                    "服务器忙，请稍候再试..",
                                    {
                                        icon: 5,
                                        time: 2000,
                                    }
                                );
                                $scope.subDisable = true;
                            }
                        );
                    }
                })
            }
        }
        /*添加维修记录结束*/
    }
});