angular.module('myApp.car')
.component('addCarComponent',{
    templateUrl:'car_module/add_car.html',
    css:'car_module/add_car.css',
    controller: function ($scope, $location, $http, $state) {

        console.log("This is add_car componenet")
        $scope.subDisable = true;   
        //layui日期选择
        layui.laydate.render({
            elem: '#carDatestart',
            type: 'date', //默认，可不填
            theme: '#00722e',
            max: 0,
            // change: function (value) {
            //     console.log(value); //得到日期生成的值，如：2017-08-18
            //     console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            //     console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
            // },
            done: function (value) {
                console.log(value); //得到日期生成的值，如：2017-08-18
                $scope.obj.create_time = value;
            }
        });
        // 确定添加车辆
        $scope.obj = {};
        $scope.num_head = '京'; // 车牌默认京
        $scope.province = false; // 车牌选择框隐藏
        $scope.selectN = function () { // 隐藏显示车牌选择框
            if($scope.province == false){
                $scope.province = true;
            }else{
                $scope.province = false;
            }
        };

        $scope.numselect = function (num) { // 选择车牌
            $scope.num_head = num;
            $scope.province = false;
        };
        //设定车类型
        $scope.car_type = [{
                value: '1',
                text: '轿车'
            },
            {
                value: '2',
                text: '运输车'
            },
            {
                value: '3',
                text: '挖掘机'
            },
            {
                value: '4',
                text: '渣土车'
            },
            {
                value: '0',
                text: '其他车'
            }
        ];
        $scope.typeOpShow = false;
        $scope.isTypeShow = function () {
            if (!$scope.typeOpShow) {
                $scope.typeOpShow = !$scope.typeOpShow;
            } else {
                $scope.typeOpShow = !$scope.typeOpShow;
            }
        }
        $scope.choseType = function (item) {
            $('.select-type').html(item.text);
            // $scope.cartypeText = item.text;
            $scope.obj.cartype = item.value;
            console.log($scope.obj.cartype)
            $scope.typeOpShow = !$scope.typeOpShow;
        }
        $scope.reg = /^[A-Za-z]{1}[A-Za-z0-9]{4}[A-Za-z0-9挂学警港澳]{1}$/;
        //再次检测是否为纯数字
        $scope.carNumTest = /^[a-zA-Z]{6}$/
        $scope.costs=/^[1-9][0-9]{1,7}$/;
        $scope.tNum = /^[a-zA-Z0-9]{8,15}$/;
        $scope.addCar = function(){
            $scope.obj.create_time = $(".datestart").val();
            if ($scope.obj.cartype == null || $scope.obj.cartype === '') {
                layer.msg("车型号不能为空", {
                    icon: 5,
                    time: 2000, //20s后自动关闭
                    // btn: ['确定']
                });
                return;
            }
            if ($scope.num_body == null || $scope.num_body === '') {
                layer.msg("车牌号不能为空", {
                    icon: 5,
                    time: 2000, //20s后自动关闭
                    // btn: ['确定']
                });
                return;
            } else if (!$scope.reg.test($scope.num_body)) {
                layer.msg(
                    "您填写的车牌号格式不正确",
                    {
                        icon:5,
                        time:2000
                    }
                )
                return;
            } else if ($scope.carNumTest.test($scope.num_body)) {
                layer.msg(
                    "车牌号不能为纯字母", {
                        icon: 5,
                        time: 2000
                    }
                )
                return;
            }
            else if ($scope.obj.create_time == null || $scope.obj.create_time === '') {
                layer.msg("车辆生产日期不能为空", {
                    icon: 5,
                    time: 2000, 
                });
                return;
            }else if ($scope.obj.terminal_num == null || $scope.obj.terminal_num === '') {
                layer.msg("终端号不能为空", {
                    icon: 5,
                    time: 2000, 
                });
                return;
            } else if (!$scope.tNum.test($scope.obj.terminal_num)) {
                layer.msg("您填写的终端号码格式不正确", {
                    icon: 5,
                    time: 2000,
                });
                return;
            }else if ($scope.obj.rent_money == null || $scope.obj.rent_money === '') {
                layer.msg("服务价格不能为空", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if (!$scope.costs.test($scope.obj.rent_money)) {
                layer.msg(
                    "您填写的价格格式不正确",
                    {
                        icon:5,
                        time:2000
                    }
                );
                return;
            }else{
                $scope.subDisable = false;
                $scope.obj.car_num = $scope.num_head + $scope.num_body.toUpperCase();
                $http.post(baseurl+ '/add_car/carinfo', $scope.obj).then(function(res){
                    console.log(res);
                    console.log($scope.obj);
                    if (res.data.ret === 1) {
                        layer.msg("添加成功", {
                            icon: 1,
                            time: 2000, 
                            },
                            function(index){
                                $state.reload();
                                $scope.subDisable = true;
                                layer.close(index);
                            }
                        );
                    }else if(res.data.ret===-4){
                        layer.msg("添加失败,该车已存在!", {
                            icon: 2,
                            time: 2000,
                        });
                    }else if(res.data.ret===-5){
                        layer.msg("添加失败,该终端号已存在!", {
                            icon: 2,
                            time: 2000,
                        });
                    } else if (res.data.ret === -1){
                        layer.msg("添加失败，车牌号或终端号已存在..", {
                            icon: 2,
                            time: 2000,
                        });
                    } else if (res.data.ret === -3){
                        layer.msg("身份验证失败，请重新登录..", {
                            icon: 4,
                            time: 2000,
                        },
                        function(index){
                            window.location.href = URL.baseurl + 　'login.html';
                            layer.close(index);
                        }
                    );
                    }
                    $scope.subDisable = true;
                },
                (err)=>{
                    console.log(err);
                    layer.msg("服务器忙，请稍后重试..", {
                        icon: 5,
                        time: 2000,
                    });
                    $scope.subDisable = true;
                });
            }
        };

        $scope.privence = ["京","津","沪","渝","蒙","新","藏","宁","桂","港","澳","黑","吉","辽","晋","冀","青","鲁","豫","苏","皖","浙","闽","赣","湘","鄂","粤","琼","甘","陕","贵","云","川"]

        // $('#carDatestart').jHsDate();

        // $(".choice-date").on("click", function (e) {
        //     e = window.event || e; // 兼容IE7
        //     obj = $(e.srcElement || e.target);
        //     if (!$(obj).is("[name='jHsDateInput']") && !$(obj).is('[class^="hs_"]')) {
        //         $('[name="jHsDate"]').remove();
        //     }
        // })
        function selectN(){
            if($(".province_add").is(":hidden")){
                $(".province_add").show()
            }else{
                $(".province_add").hide()
            }
        }
        /*添加车辆信息结束*/
    }
})