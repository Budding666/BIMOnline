angular.module('myApp.car')
.component('carListComponent',{
    templateUrl:'car_module/car_list.html',
    css:'car_module/car_list.css',
    controller: function ($scope, $http, $state, $stateParams, $cookieStore) {

        console.log("This is car_list component")

        //加载车辆列表信息 
        //car_list页面搜索功能
        $scope.reg = /([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领]{1}[A-Z]{1}(([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1})/;       
        $scope.searchCar = function () {
            var carNum = $('#searchCarNum').val().toUpperCase();
            console.log(carNum);
            if(carNum===null||carNum===""){
                layer.msg(
                    "车牌号不能为空", {
                        icon: 7,
                        time: 2000
                    }
                )
                return;
            } else if (!$scope.reg.test(carNum)){
                layer.msg(
                    "您输入的车牌号码格式不正确", {
                        icon: 7,
                        time: 2000
                    }
                )
                return;
            }else{
                 $http.post(baseurl + "/get_one_car?car_num="+carNum).then(
                     (res)=>{
                        console.log(res);
                        if(res.data.ret===1){
                            $scope.infos = res.data.info;
                            switch ($scope.infos.cartype) {
                                case '0':
                                    $scope.infos.cartype = '其它车';
                                    break;
                                case '1':
                                    $scope.infos.cartype = '轿车';
                                    break;
                                case '2':
                                    $scope.infos.cartype = '运输车';
                                    break;
                                case '3':
                                    $scope.infos.cartype = '挖掘机';
                                    break;
                                case '4':
                                    $scope.infos.cartype = '渣土车';
                                    break;
                            }
                            $('.loadmore').css('display', 'none');
                            $('.car_li').css('display', 'none');
                            $('.searchResult').css('display', 'block');
                        }else if(res.data.ret===-1){
                            layer.msg(
                                "您查找的车辆不存在..", {
                                    icon: 7,
                                    time: 2000
                                }
                            );
                        }else if(res.data.ret===-3){
                            layer.msg(
                                "身份验证失败，请重新登录..", {
                                    icon: 4,
                                    time: 2000
                                }
                            )
                        }
                     },
                     (err)=>{
                        console.log(err);
                        layer.msg(
                            "服务器维护中，请稍候再试..", {
                                icon: 5,
                                time: 2000
                            }
                        )
                     }
                 );

            }
            //模拟数据测试
            // $scope.data = {
            //     carNum: '陕B 12345',
            //     carName: '梅赛德斯-奔驰 CL200',
            //     date: '2018/05/13',
            //     terminalNum: '123352',
            //     servicePrice: '600'
            // }
        };

        //搜索页面返回按钮功能
        $scope.returnBtn = function () {
            console.log('返回');
            // if ($('.edit-mask').css('display') == 'none') {
            //     $('.edit-mask').css('display', 'block');
            // } else {
            //     $('.edit-mask').css('display', 'none');
            // }
            $('.edit-mask').css('display', 'none');
            $('.searchResult').css('display', 'none');
            $('.car_li').css('display', 'block');
            $('.loadmore').css('display', 'block'); 
        }
        /***********************************后期用服务进行包装**************************************/
        // 加载车辆列表
        $scope.list=[];
        $scope.showCarList=function(pg,index){
            var obj={page:pg,rows:2};
            $http.post(baseurl + "/list2/select/carinfo", obj).then(
                (res)=>{
                    if(res.data.ret===1){
                        if (res.data.rows) {
                            $scope.items = res.data.rows;
                             //转换车类型
                             $scope.items.forEach(v => {
                                 switch (v.cartype) {
                                     case '0':
                                         v.cartype = '其它车';
                                         break;
                                     case '1':
                                         v.cartype = '轿车';
                                         break;
                                     case '2':
                                         v.cartype = '运输车';
                                         break;
                                     case '3':
                                         v.cartype = '挖掘机';
                                         break;
                                     case '4':
                                         v.cartype = '渣土车';
                                         break;
                                 }
                                 console.log("-->>>>>>>", v.cartype)
                             });
                            //判断是否加载到最后一条
                            if (res.data.rows < obj.rows) {
                                $('.loadmore').html("没有更多了");
                            }
                            if ($scope.items) {
                                console.log($scope.items);
                                $scope.list.push(...$scope.items);
                                $('.loadmore').css('display', 'block');
                                if ($scope.list) {
                                    console.log($scope.list);
                                }
                            }
                            //关闭loading动画
                            $('.loadmore').css('display', 'block');
                            $('.loading-img').css('display', 'none');
                        } else {
                            layer.msg("暂无车辆信息！请点击添加车辆按钮添加信息..", {
                                icon: 7
                            });
                            // console.log("无车辆信息！")
                        }
                    }
                },
                (err)=>{
                    console.log(err);
                    $('.loadmore').css('display', 'none');                                            
                }
            )
        }
        $scope.showCarList(1);
        //加载更多
        var page = 1;
        $scope.loadMore = function () {
            //从第2页开始加载
            page++;
            //开启加载动画
            $('.loadmore').css('display', 'none');
            $('.loading-img').css('display', 'block');

            $scope.showCarList(page);
            return page;
        };

        //编辑按钮功能
        $scope.carEdit=function(){
            console.log('点击了carEdit');
            if ($('.edit-mask').css('display')=='none'){
                $('.edit-mask').css('display','block');
                $('.add-car-butt').html("取消删除");
            }else{
                $('.edit-mask').css('display', 'none');
                $('.add-car-butt').html("删除车辆");
            }
        }

        //删除车辆功能
        $scope.deleteCar=function(carId){
            console.log("点击了删除deleteCar");
            console.log(carId);
            layer.confirm('此操作会永久删除该车辆信息，是否确认删除？', {
                icon: 7,
                title: '警告',
                btn: ["确定","取消"]
                }, 
                function (index) {
                    //开始删除
                    $http.delete(baseurl + '/delete_car/carinfo?id=' + carId).then(
                        (res) => {
                            if (res.data.ret === 1) {
                                console.log(res);
                                layer.msg("删除成功！",{icon:1})
                                $state.reload();
                            } else if (res.data.ret === -3) {
                                layer.msg(
                                    "身份验证失败，请重新登录！", {
                                        icon: 4,
                                        title: "提示",
                                        time: 2000,
                                    },
                                    function (index) {
                                        window.location.href = URL.baseurl + 　'login.html';
                                        layer.close(index);
                                    }
                                )
                            }
                        },
                        (err) => {
                            console.log(err);
                            layer.msg("服务器忙，请稍候重试..",{icon:5});
                            }
                    )
                    layer.close(index);
                })     
        }

        $scope.toTrail = function (carId) {
            console.log("car_list发送参数-trail:", carId);
            $state.go('trail', {carId: carId});
        }

        //清除driver_list页已缓存的car_id
        // $cookieStore.remove('car_id');
        localStorage.removeItem('car_id');

        //清除trail_map页面已缓存的trail_car_id 和 trail_car_num
        // $cookieStore.remove('trail_car_id');
        // $cookieStore.remove('trail_car_num');
        localStorage.removeItem('trail_car_id');
        localStorage.removeItem('trail_car_num');

        //清除maintain_info页面已缓存的maintain_carNum
        // $cookieStore.remove('maintain_car_num');
        localStorage.removeItem('maintain_car_num');

        //清除edit_car_info页面已缓存的edit_carInfo_op
        localStorage.removeItem('edit_carInfo_op');
        
}
})