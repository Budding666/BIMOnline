angular.module('myApp.car')
    .component('carList2Component', {
        templateUrl: 'car_module/car_list2.html',
        css: 'car_module/car_list2.css',
        controller: function ($scope, $http, $state, $stateParams, $cookieStore) {
            // 加载车辆列表
            $scope.list = [];
            $scope.showCarList = function (pg, index) {
                var obj = {
                    page: pg,
                    rows: 2
                };
                $http.post(baseurl + "/list2/select/carinfo", obj).then(
                    (res) => {
                        if (res.data.rows) {
                            $scope.items = res.data.rows;
                            //转换车类型
                            $scope.items.forEach(v => {
                                switch (v.cartype) {
                                    case '0':
                                        v.cartype = '其它车';
                                        break;
                                    case '1':
                                        v.cartype = '轿车';
                                        break;
                                    case '2':
                                        v.cartype = '运输车';
                                        break;
                                    case '3':
                                        v.cartype = '挖掘机';
                                        break;
                                    case '4':
                                        v.cartype = '渣土车';
                                        break;
                                }
                                console.log("-->>>>>>>", v.cartype)
                            });
                            if (res.data.rows < obj.rows) {
                                $('.loadmore').html("没有更多了");
                            }
                            if ($scope.items) {
                                console.log($scope.items);
                                $scope.list.push(...$scope.items);
                                $('.loadmore').css('display', 'block');
                                if ($scope.list) {
                                    console.log($scope.list);
                                }
                            }
                            //关闭loading动画
                            $('.loadmore').css('display', 'block');
                            $('.loading-img').css('display', 'none');
                        } else {
                            layer.msg("暂无车辆信息！", {
                                icon: 7
                            });
                            // console.log("无车辆信息！")
                        }
                    },
                    (err) => {
                        console.log(err);
                        $('.loadmore').css('display', 'none');
                    }
                )
            }
            $scope.showCarList(1);
            //加载更多
            var page = 1;
            $scope.loadMore = function () {
                //从第2页开始加载
                page++;
                //开启加载动画
                $('.loadmore').css('display', 'none');
                $('.loading-img').css('display', 'block');

                $scope.showCarList(page);
                return page;
            };
            //清除driver_list页已缓存的car_id
            // $cookieStore.remove('car_id');

            //清除trail_map页面已缓存的trail_car_id 和 trail_car_num
            // $cookieStore.remove('trail_car_id');
            // $cookieStore.remove('trail_car_num');

            //清除maintain_list页面已缓存的maintain_carNum
            $cookieStore.remove('maintain_car_num');

            //清除edit_car_info页面已缓存的edit_carInfo_op
            // localStorage.removeItem('edit_carInfo_op');

        }
    })