angular.module('myApp.driver')
.component('addDriverComponent',{
    templateUrl:'driver_module/add_driver.html',
    css:'driver_module/add_driver.css',
    controller:function($scope,$http,$stateParams,$state){
        // console.log("this is driver controller");
       $scope.subDisable = true;
        console.log(localStorage.getItem('car_id'))
        //在点击上传框显示用户选择要上传的头像图片
        $('#imgFile').on("change", function () {
            console.log("添加图片");

            //查看图片路径
            // var imgPath = $("#imgFile").val();
            // console.log(imgPath);
            // $(".uploaded-Img").attr('src', imgPath);
            // $(".uploaded-Img").css('display','block');
            // 获取文件
            //判断文件类型
            // var reg = new RegExp('.(gif|jpg|jpeg|png|GIF|JPG|bmp)$');
            var file = $(".addDriver_form").find("input[type=file]")[0].files[0];
            // if (reg.test(file.name)){
                console.log(file);
                //创建文件读取相关的变量 
                var imgFile;
                // 创建读取文件的对象  
                var reader = new FileReader();

                //为文件读取成功设置事件  
                reader.onload = function (e) {
                    var imgFile = e.target.result;
                    // console.log(imgFile);
                    $(".uploaded-Img").attr('src', imgFile);
                    $(".uploaded-Img").css('display', 'block');
                    // $(".uploaded-Img").css('display', '9999');

                };
                //正式读取文件  
                reader.readAsDataURL(file);
            // }
            // else{
            //     layer.msg(
            //         "您上传的文件格式不正确，请上传图片文件..",
            //         {
            //             icon:7,
            //             time:3000
            //         },
            //     )
            // }
            
        });
        //用户更换头像功能
        $('.uploaded-Img').hover(
            ()=>{
            $('.uploaded-Img').css('display','none');
            $('#imgFile').val('');
            },
            ()=>{
                console.log("mouseout");
            }
        )
        // $('.uploaded-Img').mouseover(function(){
        //     console.log("111")
        //     $('.imgMask').css('display','block');
        //     $('.uploaded-Img').css('z-index','-2');
        //     // $('.add-driverImg:span').css('z-index','-1');
        // });
        
        $scope.reg = new RegExp("^1[3|4|5|7|8][0-9]{9}$");
        $scope.salReg=/^[1-9]{1}[0-9]{1,8}$/;
        $scope.userId = /^[\u4e00-\u9fa5]{2,7}$/;
        $scope.age = /^1[8-9]{1}$|^[2-5]{1}[0-9]{1}$/;
        $scope.driverInfo_upload= function(){
            console.log('点击了确定上传');
            if ($scope.driverName == null || $scope.driverName == '') {
                layer.msg("司机姓名不能为空", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if (!$scope.userId.test($scope.driverName)) {
                layer.msg("司机姓名不符合规定的姓名格式", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if ($scope.driverPhone == null || $scope.driverPhone == '') {
                layer.msg("司机电话不能为空", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if (!$scope.reg.test($scope.driverPhone)){
                layer.msg("请输入正确的电话号码", {
                    icon: 5,
                    time: 2000, 
                });
                return;
            }
            else if ($scope.driverAge == null || $scope.driverAge == '') {
                layer.msg("司机年龄不能为空", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if (!$scope.age.test($scope.driverAge)) {
                layer.msg("司机年龄不符合规定年龄", {
                    icon: 5,
                    time: 2000,
                });
                return;                
            }else if ($scope.driverSalary == null || $scope.driverSalary == '') {
                layer.msg("司机工资不能为空", {
                    icon: 5,
                    time: 2000,
                });
                return;
            } else if (!$scope.salReg.test($scope.driverSalary)) {
                layer.msg("您输入的工资不符合规定工资", {
                    icon: 5,
                    time: 2000,
                });
                return;
            }
            // else if ($scope.workingTime == null || $scope.workingTime == '') {
            //     layer.msg("司机工作时间不能为空", {
            //         icon: 5,
            //         time: 2000,
            //     });
            //     return;
            // }
            else{
            //处理上传的图片
            $scope.subDisable = false;            
            var file = $(".addDriver_form").find("input[type=file]")[0].files[0];
            if(file){
                // console.log('用户上传的图片是：'+file);
                var form = new FormData(); // FormData 对象
                form.append("file", file); // 文件对象
                //图片转存
                $.ajax({
                    url: "http://sxzd365.com:9060/photos/upload_shop",
                    type: "POST",
                    async: true,
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        //提交之前禁用上传图像input
                        $('#imgFile').attr('disabled','disabled');
                        console.log(res);
                        //表单数据绑定
                        $scope.op = {
                            car_id: localStorage.getItem('car_id'),
                            name: $scope.driverName,
                            id: $scope.driverPhone,
                            age: $scope.driverAge,
                            // work_time: $scope.workingTime,
                            //司机工作时间暂定为空字符串
                            work_time:"",
                            wage: $scope.driverSalary,
                            driver_pic :res.result
                        }
                        console.log($scope.op);
                        //发送至后台存储
                        $http.post(baseurl + '/add_driver', $scope.op).then(
                            (res)=>{
                                console.log($scope.op);
                                console.log(res);                                
                                if(res.data.ret === 1){
                                    console.log("添加成功！");
                                    layer.msg(
                                        '司机信息添加成功！',
                                        {
                                            icon:1,
                                            title:"提示",
                                            time:2000
                                        },
                                        function(index){
                                            //跳转刷新拉取数据
                                            $scope.subDisable = true;
                                            $state.go('driver_list');
                                            layer.close(index);
                                        }
                                    );
                                   
                                }else if(res.data.ret === -3){ 
                                    layer.msg(
                                        "身份验证失败，请重新登录！", {
                                            icon: 4,
                                            title: "提示",
                                            time: 2000,
                                        },
                                        function (index) {
                                            $scope.subDisable = true;
                                            window.location.href = URL.baseurl + 　'login.html';
                                            layer.close(index);
                                        }
                                    )
                                } else if (res.data.ret === -1){
                                    if(res.data.errcode===2){
                                        layer.msg(
                                            '司机姓名或手机号已存在...', {
                                                icon: 6,
                                                title: "温馨提示",
                                                time: 2000
                                            },
                                            function (index) {
                                                console.log("温馨提示");
                                                $scope.subDisable = true;
                                                layer.close(index);
                                            }
                                        );
                                    $('#imgFile').attr('disabled', false);
                                    }else{
                                        layer.msg(
                                            '司机姓名或手机号已存在...', {
                                                icon: 6,
                                                title: "温馨提示",
                                                time: 2000
                                            },
                                            function (index) {
                                                console.log("温馨提示");
                                                $scope.subDisable = true;
                                                layer.close(index);
                                            }
                                        );
                                    $('#imgFile').attr('disabled', false);
                                    }
                                }
                                $scope.subDisable = true;
                            },
                            (err)=>{
                                console.log(err);
                                layer.msg("服务器忙，请稍候重试..", {
                                    icon: 5
                                },
                                function (index) {
                                    $scope.subDisable = true;
                                    layer.close(index);
                                });
                                $scope.subDisable = true;
                                $('#imgFile').attr('disabled', false);
                            }
                        )
                    },
                    error: function (err) {
                        $('#imgFile').attr('disabled',false);
                        console.log(err);
                        layer.msg("网络连接失败,稍后重试..", {
                            icon: 5
                        });
                        $scope.subDisable = true;
                    }
                })}else{
                    $('#imgFile').attr('disabled', 'disabled');
                    console.log("用户没有上传图片")
                    //为用户设置默认头像
                    $scope.op = {
                        car_id: $stateParams.car_num,
                        name: $scope.driverName,
                        id: $scope.driverPhone,
                        age: $scope.driverAge,
                        // work_time: $scope.workingTime,
                        //暂定为工作时间为空字符串
                        work_time: '',                        
                        wage: $scope.driverSalary,
                        driver_pic: ''
                    }

                    //发送至后台添加司机信息
                    $http.post(baseurl + '/add_driver', $scope.op).then(
                        (res) => {
                            console.log(res);
                            if (res.data.ret === 1) {
                                console.log("添加成功！");
                                layer.msg(
                                    '司机信息添加成功！', {
                                        icon: 1,
                                        title: "提示",
                                        time: 2000
                                    },
                                    function (index) {
                                        //跳转刷新拉取数据
                                        $state.go('driver_list');
                                        $scope.subDisable = true;
                                        layer.close(index);
                                    }
                                );
                            } else if (res.data.ret === -3) {
                                layer.msg(
                                    "身份验证失败，请重新登录！", {
                                        icon: 4,
                                        title: "提示",
                                        time: 2000,
                                    },
                                    function (index) {
                                        window.location.href = URL.baseurl + 　'login.html';
                                        layer.close(index);
                                    }
                                )
                            } else if (res.data.ret === -1) {
                                if(res.data.errcode===2){
                                    $scope.subDisable = true;
                                     layer.msg(
                                         '司机姓名或手机号已存在...', {
                                             icon: 6,
                                             title: "温馨提示",
                                             time: 2000
                                         },
                                         function (index) {
                                             console.log("温馨提示");
                                             layer.close(index);
                                         }
                                     );
                                    $('#imgFile').attr('disabled', false);
                                }else{
                                    $scope.subDisable = true;
                                    layer.msg(
                                        '司机姓名或手机号已存在...', {
                                            icon: 6,
                                            title: "温馨提示",
                                            time: 2000
                                        },
                                        function (index) {
                                            console.log("温馨提示");
                                            layer.close(index);
                                        });
                                    $('#imgFile').attr('disabled', false);
                                }
                            }
                            $scope.subDisable = true;
                        },
                        (err) => {
                            $('#imgFile').attr('disabled', false);
                            $scope.subDisable = true;
                            layer.msg("服务器忙，请稍候重试..", {
                                icon: 5
                            },
                            function(index){
                                layer.close(index);
                            }
                        );
                            console.log(err);
                        }
                    )
                }
            }

        }
        
    }

})