angular.module('myApp.driver', [
    'ui.router',
    'angularCSS'
])
.config(function($stateProvider){

    $stateProvider.state('driver_list',{
        params: { car_id: null },
        url:'/driver_list',
        component:'dirverListComponent'
    });

    $stateProvider.state('add_driver',{
        params:{car_num:null},
        url:'/add_driver',
        component:'addDriverComponent'
    });

    $stateProvider.state('edit_driver',{
        params: {
            car_id:null,
            name:null,
            id:null,
            age:null,
            wage:null,
            work_time:null,
            z_id:null,
            driver_pic:null,
        },
        url:'/edit_driver',
        component:'editDriverComponent'
    });

})
.service("cacheParams", function(){
    this.params = '';
    this.setParams = function(params){
        this.params = params
    }
    this.getParams = function(){
        return this.params;
    }
})