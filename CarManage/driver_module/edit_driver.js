angular.module('myApp.driver')
.component('editDriverComponent',{
    templateUrl:'driver_module/edit_driver.html',
    css:'driver_module/edit_driver.css',
    controller:'editDriverCtl'
})
.controller('editDriverCtl', function ($scope, $http, $state, $stateParams) {
    $scope.subDisable = true;
    $scope.car_id = $stateParams.car_id;
    $scope.z_id=$stateParams.z_id,
    $scope.driver = {
            name: $stateParams.name,
            id: $stateParams.id,
            age: $stateParams.age,
            wage: $stateParams.wage,
            work_time: $stateParams.work_time,
            driver_pic: $stateParams.driver_pic,
    }
    console.log($scope.car_id);
    console.log($scope.driver);
    //更换头像
    $('#imgFile').on("change", function () {
        console.log("添加图片");

        //查看图片路径
        // var imgPath = $("#imgFile").val();
        // console.log(imgPath);
        // $(".uploaded-Img").attr('src', imgPath);
        // $(".uploaded-Img").css('display','block');
        // 获取文件
        //判断文件类型
        // var reg = new RegExp('.(gif|jpg|jpeg|png|GIF|JPG|bmp)$');
        var file = $(".addDriver_form").find("input[type=file]")[0].files[0];
        // if (reg.test(file.name)) {
            console.log(file);
            //创建文件读取相关的变量 
            var imgFile;
            // 创建读取文件的对象  
            var reader = new FileReader();

            //为文件读取成功设置事件  
            reader.onload = function (e) {
                var imgFile = e.target.result;
                // console.log(imgFile);
                $(".uploaded-Img").attr('src', imgFile);
                $(".uploaded-Img").css('display', 'block');
                // $(".uploaded-Img").css('display', '9999');

            };
            //正式读取文件  
            reader.readAsDataURL(file);
        // } else {
        //     layer.msg(
        //         "您上传的文件格式不正确，请上传图片文件..", {
        //             icon: 7,
        //             time: 3000
        //         },
        //     )
        // }

    });
    //用户更换头像功能
    $('.uploaded-Img').hover(
        () => {
            $('.uploaded-Img').css('display', 'none');
            $('#imgFile').val('');
        }
    )

    $scope.reg = new RegExp("^1[3|4|5|7|8][0-9]{9}$");
    $scope.salReg = /^[1-9]{1}[0-9]{1,8}$/;
    $scope.userId = /^[\u4e00-\u9fa5]{2,7}$/;
    $scope.age = /^1[8-9]{1}$|^[2-5]{1}[0-9]{1}$/;
    $scope.edif_driverInfo = function () {
        console.log('点击了确定上传');
        //提交之前禁用上传图像input
        $('#imgFile').attr('disabled', 'disabled');
        if ($scope.driver.name === null || $scope.driver.name === '') {
            layer.msg("司机姓名不能为空", {
                icon: 5,
                time: 2000,
            });
            return;
        } else if (!$scope.userId.test($scope.driver.name)) {
            layer.msg("司机姓名不符合规定的姓名格式", {
                icon: 5,
                time: 2000,
            });
            return;
        }else if ($scope.driver.id === null || $scope.driver.id === '') {
            layer.msg("司机电话不能为空", {
                icon: 5,
                time: 2000,
            });
            return;
        } else if (!$scope.reg.test($scope.driver.id)) {
            layer.msg("您填写的电话号码格式不正确", {
                icon: 5,
                time: 2000
            });
            return;
        }else if ($scope.driver.age === null || $scope.driver.age === '') {
            layer.msg("司机年龄不能为空", {
                icon: 5,
                time: 2000,
            });
            return;
        } else if (!$scope.age.test($scope.driver.age)) {
            layer.msg("您填写的司机年龄格式不正确", {
                icon: 5,
                time: 2000,
            });
            return;
        }else if ($scope.driver.wage === null || $scope.driver.wage === '') {
            layer.msg("司机工资不能为空", {
                icon: 5,
                time: 2000,
            });
            return;
        } else if (!$scope.salReg.test($scope.driver.wage)) {
            layer.msg("您填写的司机工资格式不正确", {
                icon: 5,
                time: 2000,
            });
            return;
        } 
        // else if ($scope.driver.work_time === null || $scope.driver.work_time === '') {
        //     layer.msg("司机工作时间不能为空", {
        //         icon: 5,
        //         time: 2000,
        //     });
        //     return;
        // } 
        else {
            $scope.subDisable = false;
            //处理上传的图片
            var file = $(".addDriver_form").find("input[type=file]")[0].files[0];
            console.log("this is img file:",file)
            var imgUrl = 'https://s1.ax2x.com/2018/05/15/x530n.jpg';
            if (file) {
                // console.log('用户上传的图片是：'+file);
                var form = new FormData(); // FormData 对象
                form.append("file", file); // 文件对象
                //图片转存
                $.ajax({
                    url: "http://sxzd365.com:9060/photos/upload_shop",
                    type: "POST",
                    async: true,
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        console.log(res);
                        //表单数据绑定
                        $scope.op = {
                            name: $scope.driver.name,
                            id: $scope.driver.id,
                            age: $scope.driver.age,
                            // work_time: $scope.driver.work_time,
                            work_time:'',
                            wage: $scope.driver.wage,
                            driver_pic: res.result
                        }
                        console.log($scope.op);
                        //发送至后台存储
                        $http.put(baseurl + '/edit_car/user?z_id=' + $scope.z_id, $scope.op).then(
                            (res) => {
                                console.log($scope.op);
                                console.log(res);
                                if (res.data.ret === 1) {
                                    console.log("添加成功！");
                                    layer.msg(
                                        '司机信息修改成功！', {
                                            icon: 1,
                                            title: "提示",
                                            time: 2000
                                        },
                                        function (index) {
                                            $scope.subDisable = true;
                                            //跳转刷新拉取数据
                                            $state.go('driver_list');                                                    
                                            layer.close(index);
                                        }
                                    );
                                } else if (res.data.ret === -3) {
                                    layer.msg(
                                        "身份验证失败，请重新登录！", {
                                            icon: 4,
                                            title: "提示",
                                            time: 2000,
                                        },
                                        function (index) {
                                            window.location.href = URL.baseurl + 　'login.html';
                                            layer.close(index);
                                        }
                                    )
                                } else if (res.data.ret === -1) {
                                    if(res.data.errcode===2){
                                         layer.msg(
                                             '司机姓名或手机号已存在...', {
                                                 icon: 6,
                                                 title: "温馨提示",
                                                 time: 2000
                                             },
                                             function (index) {
                                                 console.log("温馨提示");
                                                 layer.close(index);
                                             }
                                         );
                                    $('#imgFile').attr('disabled', false);
                                    }else{
                                        layer.msg(
                                            '司机姓名或手机号已存在...', {
                                                icon: 6,
                                                title: "温馨提示",
                                                time: 2000
                                            },
                                            function (index) {
                                                console.log("温馨提示");
                                                $scope.subDisable = true;
                                                layer.close(index);
                                            }
                                        );
                                        $('#imgFile').attr('disabled', false);
                                    }
                                }
                                $scope.subDisable = true;
                            },
                            (err) => {
                                $('#imgFile').attr('disabled', false);
                                console.log(err);
                                layer.msg("服务器忙，请稍候重试..", {
                                    icon: 5
                                });
                                $scope.subDisable = true;
                            }
                        )
                    },
                    error: function (err) {
                        $('#imgFile').attr('disabled', false);
                        alert("网络连接失败,稍后重试", err);
                    }
                })
            } else {
                if ($scope.driver.driver_pic !== imgUrl) {
                    console.log("用户没有更换图片");
                    //表单数据绑定
                    $scope.op = {
                        name: $scope.driver.name,
                        id: $scope.driver.id,
                        age: $scope.driver.age,
                        // work_time: $scope.driver.work_time,
                        work_time: '',
                        wage: $scope.driver.wage,
                        driver_pic: $scope.driver.driver_pic
                    }
                    console.log($scope.op);
                    //发送至后台存储
                    $http.put(baseurl+'/edit_car/user?z_id=' + $scope.z_id, $scope.op).then(
                        (res) => {
                            console.log($scope.op);
                            console.log(res);
                            if (res.data.ret === 1) {
                                console.log("添加成功！");
                                layer.msg(
                                    '司机信息修改成功！', {
                                        icon: 1,
                                        title: "提示",
                                        time: 2000
                                    },
                                    function (index) {
                                        //跳转刷新拉取数据
                                        $scope.subDisable = true;
                                        $state.go('driver_list');
                                        layer.close(index);
                                    }
                                );

                            } else if (res.data.ret === -3) {
                                layer.msg(
                                    "身份验证失败，请重新登录！", {
                                        icon: 4,
                                        title: "提示",
                                        time: 2000,
                                    },
                                    function (index) {
                                        $scope.subDisable = true;
                                        window.location.href = URL.baseurl + 　'login.html';
                                        layer.close(index);
                                    }
                                )
                            } else if (res.data.ret === -1) {
                                if(res.data.errcode===2){
                                    layer.msg(
                                        '司机姓名或手机号已存在...', {
                                            icon: 6,
                                            title: "温馨提示",
                                            time: 2000
                                        },
                                        function (index) {
                                            console.log("温馨提示");
                                            layer.close(index);
                                        }
                                    );
                                    $('#imgFile').attr('disabled', false);
                                }else{
                                    layer.msg(
                                        '司机姓名或手机号已存在...', {
                                            icon: 6,
                                            title: "温馨提示",
                                            time: 2000
                                        },
                                        function (index) {
                                            console.log("温馨提示");
                                            layer.close(index);
                                        }
                                    );
                                $('#imgFile').attr('disabled', false);
                                }
                            }
                            $scope.subDisable = true;
                        },
                        (err) => {
                            $('#imgFile').attr('disabled', false);
                            console.log(err);
                            layer.msg("服务器忙，请稍候重试..", {
                                icon: 5
                            });
                            $scope.subDisable = true;
                        }
                    )
                }else{
                    console.log("用户没有上传图片");
                    //为用户设置默认头像
                    $scope.op = {
                        name: $scope.driver.name,
                        id: $scope.driver.id,
                        age: $scope.driver.age,
                        // work_time: $scope.driver.work_time,
                        //工作时间暂定为空字符串
                        work_time:"",
                        wage: $scope.driver.wage,
                        driver_pic: ''
                    }
                    console.log($scope.op)
                    //发送至后台添加司机信息
                    $http.put(baseurl + '/edit_car/user?z_id=' + $scope.z_id, $scope.op).then(
                        (res) => {
                            console.log(res);
                            if (res.data.ret === 1) {
                                console.log("添加成功！");
                                layer.msg(
                                    '司机信息修改成功！', {
                                        icon: 1,
                                        title: "提示",
                                        time: 2000
                                    },
                                    function (index) {
                                        //跳转刷新拉取数据
                                        $state.go('driver_list');
                                        layer.close(index);
                                    }
                                );
                            } else if (res.data.ret === -3) {
                                layer.msg(
                                    "身份验证失败，请重新登录！", {
                                        icon: 4,
                                        title: "提示",
                                        time: 2000,
                                    },
                                    function (index) {
                                        window.location.href = URL.baseurl + 　'login.html';
                                        layer.close(index);
                                    }
                                )
                            } else if (res.data.ret === -1) {
                                layer.msg(
                                    '司机姓名或手机号已存在...', {
                                        icon: 6,
                                        title: "温馨提示",
                                        time: 2000
                                    },
                                    function (index) {
                                        console.log("温馨提示");
                                        layer.close(index);
                                    }
                                );
                                $('#imgFile').attr('disabled', false);
                            }
                            $scope.subDisable = true;
                        },
                        (err) => {
                            $('#imgFile').attr('disabled', false);
                            layer.msg("服务器忙，请稍候重试..", {
                                icon: 5
                            });
                            console.log(err);
                            $scope.subDisable = true;
                        }
                    )
                }
            }
        }
    }
})

