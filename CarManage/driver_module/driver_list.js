angular.module('myApp.driver')
.component('dirverListComponent',{

    templateUrl:'driver_module/driver_list.html',
    css:'driver_module/driver_list.css',
    controller: function ($scope, $http, $state, $stateParams){
        //缓存传过来的car_id
        //判断是否有缓存 没有才缓存
        // if (!$cookieStore.get('car_id')){
        //     $cookieStore.put('car_id', $stateParams.car_id);
        //     console.log("cookiestore缓存的car_id" + $cookieStore.get('car_id'));
        // }
        if(!localStorage.getItem('car_id')){
            localStorage.setItem('car_id', $stateParams.car_id);
            console.log("localStorage缓存的car_id" + localStorage.getItem('car_id'));
        }
        
        $scope.car_id = localStorage.getItem('car_id');
        console.log("id为", $scope.car_id)

        $http.post(baseurl+ "/get_car_user?car_role==2", {car: $scope.car_id}).then(function (res) {
            console.log(res.data.info);
            if (res.data.ret === 1 ) {

                $scope.drivers = res.data.info;
                $scope.drivers.forEach(v => {
                    v.isShow=false;
                    console.log("this is v",v);
                });
                console.log("已有司机",$scope.drivers.length);
                    if ($scope.drivers.length >= 3) {
                        console.log("司机数量已达3个")
                        $('.footer_btn').css('display', 'none');
                        $('.no-jump').css('display', 'block');
                         $('.no-jump').on('click', () => {
                             layer.msg(
                                 "最多添加3个司机，无法再添加", {
                                     icon: 5,
                                     time: 3000
                                 }
                             )
                         })
                    }
                
               
                //为没有头像的司机添加默认头像
                for(let i=0;i<res.data.info.length;i++){
                     if (res.data.info[i].driver_pic === '' || res.data.info[i].driver_pic === null) {
                         $scope.drivers[i].driver_pic = 'https://s1.ax2x.com/2018/05/15/x530n.jpg'
                         console.log($scope.drivers[i].driver_pic);
                     }
                }
               
            } else if (res.data.ret === -3){
                layer.msg(
                    "身份验证失败，请重新登录！",
                    {
                        icon:4,
                        title:"提示",
                        time:3000,
                    },
                    function(index){
                        window.location.href = URL.baseurl + 　'login.html';                        
                        layer.close(index);
                    }
                )
            } else if(res.data.ret ===-1){
                layer.msg(
                    '暂无司机信息，请点击添加司机按钮添加司机...',
                    {
                        icon:6,
                        title:"温馨提示",
                        time:3000
                    },
                    function (index) { 
                        console.log("温馨提示");
                        layer.close(index);
                     }
                );
            }
        })
        //点击删除司机按钮显示删除司机按钮
        $scope.cancleisShow=false;
        $scope.deleteDriverBtn = function (driver) {
            if (driver.isShow) {
                $scope.cancleisShow = false;
                driver.isShow = false;
            }else{
                driver.isShow = true;
                $scope.cancleisShow = true
                
                // $scope.cancleisShow = true;
            }
        //     if ($('.deleteDriver-mask').css('display')=='none'){
        //             $('.deleteDriver-mask').css('display','block');
        //             $('.cancle-btn').css('display', 'block');
        //     }else{
        //         $('.deleteDriver-mask').css('display', 'none');
        //         $('.cancle-btn').css('display', 'none');
        //     }
        }
        //点击取消删除按钮取消删除功能
        $scope.cancleDel=function(){
            $scope.drivers.forEach(v=>{
                v.isShow=false;
            });
            $scope.cancleisShow = false;
        }
        
        //删除司机功能
        $scope.deleteDriver=function(driverId){
            console.log("点击了删除司机");
            console.log(driverId);
            //弹出confirm 防止误删除
            layer.confirm("此操作会永久删除该司机信息，是否确认删除？",{
                icon:7,
                title:"警告",
                btn:["确定","取消"],
                },
                function(index){
                    //开始删除
                    var deleOp={
                        car: $scope.car_id,
                        user_id: driverId
                    }
                    $http.delete(baseurl + "/delete_car/user?z_id==" + driverId).then(
                        (res) => {
                            if(res.data.ret===1){
                                console.log(res.data);
                                layer.msg('删除成功！', {
                                    icon: 1
                                });
                                //删除成功返回司机列表 并刷新重新拉取数据
                                $state.reload();
                            }else if(res.data.ret===-1){
                                layer.msg('删除失败，请稍候重试', {
                                    icon: 5
                                });
                            }else if(res.data.ret===-3){
                                layer.msg(
                                    "身份验证失败，请重新登录！", {
                                        icon: 4,
                                        title: "提示",
                                        time: 3000,
                                    },
                                    function (index) {
                                        window.location.href = URL.baseurl + 　'login.html';
                                        layer.close(index);
                                    }
                                )
                            }
                            
                        },
                        (err) => {
                            console.log(err);
                            layer.msg('服务器忙,请稍候重试..',{icon:5});
                        }
                    );
                    layer.close(index);
                }
            ) 
        }

        //点击司机头像放大预览
        $scope.enlargeImg=function($event){
            console.log("点击了司机头像放大");
            var img = $event.srcElement || $event.target;
            $('.show-img').attr('src',img.src);
            $('.img-mask').css('display','block');
            $('.show-img').css('display', 'block');
        }
        //关闭预览
        $scope.hideMask=function(){
            $('.img-mask').css('display', 'none');
            $('.show-img').css('display', 'none');
        }
    }

})