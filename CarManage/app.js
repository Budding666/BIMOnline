baseurl = "http://120.27.21.149:80/car";
baseurl_login = "http://120.27.21.149:80";
//???
// baseurl = "http://192.168.7.120:80/car";
// baseurl_login = "http://192.168.7.120:80";

function getBaseURL(){
    var numAtCar = document.URL.indexOf("CarManage")
    this.baseurl = document.URL.slice(0, numAtCar)
}
var URL = new getBaseURL();


angular.module('myApp', [
    'ui.router',
    'angularCSS',
    'myApp.driver',
    'myApp.LoginAndRegister',    
    'myApp.car',
    'myApp.carData',
    'myApp.server'
])
.config(function($httpProvider, $stateProvider, $urlRouterProvider) {
    $httpProvider.defaults.withCredentials = true;
    $stateProvider.state({
        name: 'home',
        url: '/home',
        component: 'homeComponent'
    });
    $stateProvider.state({
        name: 'home2',
        url: '/home2',
        component: 'home2Component'
    });
    //默认路由
    $urlRouterProvider.otherwise('login');
})
