var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('checkCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		// window.history.go(-1);
        window.location.href = 'index1.html';
	}
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	console.log(baseurls);
	$scope.getList = function(){
	    $http.get(baseurls + '/list/nopage/perworklog').then(function (res) {
			console.log(res);
			if(res.data.ret == -1){
				$(".dis_none").css("display","none");
			} else if (res.data.ret == 1) {
			    var handle_data = handle_pass(res.data.info);
				$scope.constList = handle_data;
				// $scope.constList = res.data.info
			}else if (res.data.ret == -3) {
			    window.location.href = 'login.html';
			}
		});
	};
	$scope.getList();
	//处理是否通过按钮事件
	$scope.passOrRefuse=function(obj){
		if(obj.pass=='通过'){
			layer.alert("此日志已审批通过，不需要再次审批了");
		}else if(obj.pass=="不通过"){
			sub_pass(obj);
		} else if (obj.pass == "未处理"){
			sub_pass(obj);
		}
	}
	//封装审批函数
	var sub_pass=function(obj){
		layer.confirm(
			'请对该日志进行审批', {
				title: '日志审批',
				btn: ['通过', '不通过'],
				yes: function () {
					console.log("审批通过");
					$http.put(baseurls + '/data/logwork?id==' + obj.id, {
						statue: 'agree'
					}).then(
						(res) => {
							console.log(res);
							if (res.data.ret === 1) {
								layer.msg(
									"审批提交成功", {
										icon: 1,
										time: 2000
									},
									function (index) {
										$scope.getList();
										layer.close(index)
									}
								)
							} else {
								layer.msg(
									"审批提交失败，请重试", {
										icon: 5,
										time: 2000
									},
									function (index) {
										layer.close(index)
									}
								)
							}
						},
						(err) => {
							layer.msg(
								"服务器忙，请重试", {
									icon: 7,
									time: 2000
								},
								function (index) {
									layer.close(index)
								}
							)
							console.log(err);
						}
					)
				},
				btn2: function () {
					console.log("审批不通过");
					$http.put(baseurls + '/data/logwork?id==' + obj.id, {
						statue: 'disagree'
					}).then(
						(res) => {
							console.log(res);
							if (res.data.ret === 1) {
								layer.msg(
									"审批提交成功", {
										icon: 1,
										time: 2000
									},
									function (index) {
										$scope.getList();
										layer.close(index)
									}
								)
							} else {
								layer.msg(
									"审批提交失败，请重试", {
										icon: 5,
										time: 2000
									},
									function (index) {
										$scope.getList();
										layer.close(index)
									}
								)
							}
						},
						(err) => {
							layer.msg(
								"服务器忙，请重试", {
									icon: 7,
									time: 2000
								},
								function (index) {
									layer.close(index)
								}
							)
							console.log(err);
						}
					)
				}
			}
		)
	}
	// //edit show
	// $scope.show_edit = function (obj) {
	//     $scope.editobj = obj;
	//     $scope.editid = obj.id;
	//     if (obj.statue == 'agree') {
	//         $("#btn_info").modal("show");
	//     } else {
	//         $("#editbox").modal("show");
	//     }
	// };
	// //edit sure
	// $scope.sure_edit = function () {
	//     $http.put(baseurls + '/data/logwork?id==' + $scope.editid, { statue: $scope.editobj.statue }).then(function (res) {
	// 		if (res.data.ret == 1) {
	// 		    $scope.getList();
	// 			$("#editbox").modal("hide");
	// 		    history.go(0);
	// 		}
	// 	});
	// };
	// $scope.show_image = function(obj){
	// 	$scope.imgbig = obj; 
	// 	$("#imgbox").modal("show");
	// };
	// $scope.hide_image = function(obj){
	// 	$scope.imgbig = obj; 
	// 	$("#imgbox").modal("hide");
	// }
})