var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('threeModalCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.history.go(-1);
	}
	console.log(baseurls);
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	var JSON_URL = window.location.search.replace('?json==', '');
	show_3d(JSON_URL);
	function show_3d(three_url) {
	    if (!Detector.webgl) {
	        Detector.addGetWebGLMessage();
	    }

	    var container, stats;
	    var camera, scene, renderer, objects;
	    var clock = new THREE.Clock();
	    var objects = [];
	    var raycaster;
	    // init scene
	    init();
	    //animate();
	    $("#info").modal("show");
	    var onProgress = function (xhr) {
	        if (xhr.lengthComputable) {
	            var percentComplete = xhr.loaded / xhr.total * 100;
	           // console.log(Math.round(percentComplete, 2) + '% downloaded');
	            $(".spn_progess").css('width', Math.round(percentComplete, 2) + '%');
	            $(".spn_progess").text(Math.round(percentComplete, 2) + '%');
	            setTimeout(function () {
	                if (Math.round(percentComplete, 2) == 100) {
	                    $("#info").modal("hide");
	                }
	            },2000)
	            render();
	        }
	    };
	    var onError = function (xhr) {
	        $("#error").modal("show");
	    };
	    render();
	    // Load jeep model using the AssimpJSONLoader
	    var loader1 = new THREE.AssimpJSONLoader();
	    loader1.load(three_url, function (object) {
	        console.log('object', object);
	        object.position.x = -1.5;
	        object.scale.multiplyScalar(0.008);
	        parseObject(object);
	        render();
	        $scope.go_left = function () {
	            object.position.x += -0.1;
	            render();
	        };
	        $scope.go_right = function () {
	            object.position.x += 0.1;
	            render();
	        };
	        $scope.go_up = function () {
	            object.position.y += 0.1;
	            render();
	        };
	        $scope.go_down = function () {
	            object.position.y += -0.1;
	            render();
	        };
	        scene.add(object);
	    }, onProgress, onError);

	    function init() {

	        container = document.createElement('div');
	        document.body.appendChild(container);

	        camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 2000);
	        camera.position.set(0, 4, 5);

	        scene = new THREE.Scene();

	        scene.add(new THREE.AmbientLight(0xcccccc));

	        var directionalLight = new THREE.DirectionalLight(0xeeeeee);
	        directionalLight.position.x = Math.random() - 0.5;
	        directionalLight.position.y = Math.random();
	        directionalLight.position.z = Math.random() - 0.5;
	        directionalLight.position.normalize();
	        scene.add(directionalLight);

	        // Renderer
	        renderer = new THREE.WebGLRenderer();
	        renderer.setPixelRatio(window.devicePixelRatio);
	        renderer.setSize(window.innerWidth, window.innerHeight);
	        container.appendChild(renderer.domElement);

	        var controls = new THREE.OrbitControls(camera, renderer.domElement);
	        controls.addEventListener('change', render);
	        controls.target.set(0, 0, 0);
	        //控制器极坐标系的范围
	        controls.maxPolarAngle = Math.PI / 2;
	        controls.minDistance = 10;
	        controls.maxDistance = 20;
	        controls.update();
	        raycaster = new THREE.Raycaster();
	        mouse = new THREE.Vector2();

	        //document.addEventListener('mousemove', onDocumentMouseMove, false);
	        document.addEventListener('mousedown', onDocumentMouseDown, false);
	        window.addEventListener('resize', onWindowResize, false);
	    };
	    function parseObject(obj) {

	        if (obj.children != null) {
	            for (var i = 0; i < obj.children.length; i++) {
	                objects.push(obj.children[i])
	                parseObject(obj.children[i])
	            }
	        }
	        return
	    };
	    function ObjectHidden(obj) {
	        obj.visible = false;
	        //console.log(obj.children);
	        if (obj.children != null) {
	            for (var i = 0; i < obj.children.length; i++) {
	                //console.log(i)
	                ObjectHidden(obj.children[i])
	            }
	        }
	    };
	    function onDocumentMouseDown(event) {
	        event.preventDefault();
	        mouse.x = (event.clientX / renderer.domElement.clientWidth) * 2 - 1;
	        mouse.y = -(event.clientY / renderer.domElement.clientHeight) * 2 + 1;
	        raycaster.setFromCamera(mouse, camera);
	        if (objects.length != 0) {
	            var intersects = raycaster.intersectObjects(objects);
	            if (intersects.length > 0) {
	                //intersects[ 0 ].object.material.color.setHex( Math.random() * 0xffffff );
	                //console.log(intersects[0].object)
	                ObjectHidden(intersects[0].object)
	                // intersects[ 0 ].object.visible = false
	                //使所有的子对象失效
	            }
	        }
	    };
	    function onDocumentMouseMove(event) {

	        mouseX = (event.clientX - windowHalfX) / 2;
	        mouseY = (event.clientY - windowHalfY) / 2;

	    };
	    function onWindowResize(event) {

	        renderer.setSize(window.innerWidth, window.innerHeight);

	        camera.aspect = window.innerWidth / window.innerHeight;
	        camera.updateProjectionMatrix();

	    };

	    var t = 0;
	    function animate() {

	        requestAnimationFrame(animate);

	        render();
	        stats.update();
	    };

	    function render() {

	        renderer.render(scene, camera);

	    };
	}
})