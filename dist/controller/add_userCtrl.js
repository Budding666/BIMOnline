var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('add_userCtrl', function ($scope, $http, $cookieStore) {
	//用于检测的正则变量
	$scope.userCountReg = /^[0-9a-zA-Z]{8,}$/;
	$scope.telephoneNumReg = /^1[3|4|5|7|8]{1}[0-9]{9}$/; //new RegExp("^1[3|4|5|7|8][0-9]{9}$");
	// $scope.userNameRegEN = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]\s]/;
	// $scope.userNameRegCN = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]\s]/;
	// $scope.deviceOrProjectNUmber = /^[0-9A-Za-z_-]+$/;
	$scope.go_back = function(){
        window.location.href = 'user.html';
	};
	$scope.getrole_list = function () {
	    $http.get(baseurls + '/select_idname/role').then(function (res) {
	        console.log(res.data.info)
	        if (res.data.ret == 1) {
	            $scope.constList = res.data.info;
	        } else if (res.data.ret == -3) {
	            window.location.href = 'login.html';
	        }
	    })
	}
	$scope.getrole_list();
	//管理员添加项目

    $('#file').change(function(){
    	var file = $(this)[0].files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file)
        reader.onload = function (e) {
            var imgFile = e.target.result;
			$("#usrPic").attr('src', imgFile);
			$("#usrPic").css('width','100px');
			$("#usrPic").css('height', '100px');
        };
	})

	$scope.addobj = {};
	$scope.sure_add = function () {
		// console.log("You click me")
		/************这里放置文本验证信息***************/
        if ($scope.addobj.id == null || $scope.addobj.id == '' || $scope.addobj.id == undefined) {
            $scope.errMsg = "用户账号不能为空";
            return;
		} else if (!$scope.userCountReg.test($scope.addobj.id)) {
			$scope.errMsg = "用户账号为最少8为的数字和字母组成";
			return;
		}else if ($scope.addobj.name == null || $scope.addobj.name == '' || $scope.addobj.name == undefined) {
            $scope.errMsg = "用户昵称不能为空";
            return;
        }else
        if ($scope.addobj.tele == null || $scope.addobj.tele == '' || $scope.addobj.tele == undefined) {
            $scope.errMsg = "用户电话不能为空";
            return;
        } else if (!$scope.telephoneNumReg.test($scope.addobj.tele)) {
			$scope.errMsg = "请输入正确的电话号码";
			return;
		}else
        if ($scope.addobj.roleid == null || $scope.addobj.roleid == '' || $scope.addobj.roleid == undefined) {
            $scope.errMsg = "用户角色不能为空";
            return;
        }else{
			$scope.errMsg = false;
			var $btn = $('#myButton').button('loading')
			var file = $("#file")[0].files[0];
			if( file ){
				var form = new FormData(); // FormData 对象
				//console.log(file)
				form.append("file", file); // 文件对象
				$.ajax({
					url:'http://sxzd365.com:9060/photos/upload_shop',
					type:"POST",
					async:true,
					data: form,
					processData: false,
					contentType: false,
					success:function(res){
						$scope.addobj.pic = res.result;
						PostUserInfo($scope.addobj, $btn);
					},
					error:function(err){
						console.log("This is error*********")
						console.log(err)
					}
				})
			}else{
				PostUserInfo($scope.addobj, $btn)
			}
		}
	};

	function PostUserInfo(userinfo,btn){
		$http.post(baseurls+'/data/user', userinfo).then((res)=>{
            console.log(res.data);
			if(res.data.ret == 1){
                layer.msg(
                    '添加成功！', {
                        icon: 1,
                        title: "提示",
                        time: 2000
                    },
                    function (index) {
						btn.button('reset')
                        window.location.href='./user.html';
                        layer.close(index);
                    }
                );
			}
			else if(res.data.ret == -1){
				btn.button('reset')
			}
		},(err)=>{
			console.log(err)
		})
	}
})