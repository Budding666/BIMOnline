var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('userCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.location.href = 'index1.html';
	}
	console.log(baseurls);
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	//获取用户列表
	$scope.getList = function(){
		$http.get(baseurls+'/list_nopage/user?car_role==1').then(function(res){
			if (res.data.ret == 1) {
				$scope.constList = handle_pic(res.data.info);
			}else if(res.data.ret == -1){
				$(".dis_none").css("display","none");
			} else if (res.data.ret == -3) {
			    window.location.href = 'login.html';
			}
		})
	}

	/**********************************/
	$('.submit_btn').click(function(){
        var data = new FormData($('#submit')[0]);  
        $.ajax({
            url: fileurls+'/photos/upload_shop',  
            type: 'POST',
            data: data,  
            dataType: 'JSON',
            cache: false,
            processData: false,
            contentType: false,
            success:function(data){
                console.log(data);
                alert("上传成功");
                $scope.addobj.pic = data.result;
                console.log($scope.addobj.pic);
                $scope.sure_add();
            },
             error:function(data){
                console.log(data);
                alert("上传失败");
            }
        });      
   });
	$scope.getList();
    /*************************/
	//当确认为true的时候,添加到队列当中
	function CheckBoxList(){
		this.list = new Array();
		if(typeof this.add != "function"){
            CheckBoxList.prototype.add = function(obj){
				var has = this.list.some(function(item, index, array){
					return item == obj;
				})
				if(has){
					return false;
				}else{
					this.list.push(obj);
					return true;
				}
			}
			CheckBoxList.prototype.del = function(obj){
				var index = this.list.indexOf(obj)
				if(index === -1){
					return false;
				}else{
					this.list.splice(index, 1);
					return true;
				}
			}
            CheckBoxList.prototype.clear = function(){
                this.list = [];
			}
            CheckBoxList.prototype.toString = function(){
            	var str = "[";
            	this.list.forEach(function(item, index, array){
					if(index != array.length-1){
						str += '\"' + item.z_id  + '\"' + ","
					}else{
                        str += '\"' + item.z_id  + '\"';
					}
				})
				str += "]"
				return str;
			}
		}
	}

	$scope.List = new CheckBoxList();

    /************控制用户的删除************/
    $scope.delUserFlag = false;
    $scope.delUser = function(){
        $scope.delUserFlag = !$scope.delUserFlag;
        console.log('del')
    }
    /*************需要做一次判断看是否为$scope.delUserFlag****************/
    $scope.jump_detail = function(event,obj){
        if($scope.delUserFlag){
        } else{
            window.location.href = 'userDetail.html?id='+obj.id;
        }
    }

	$scope.checkBtn = function(event, obj){
		if(event.target.checked){
            console.log("check box is true")
            if($scope.List.add(obj)){
				console.log("添加成功")
			}else{
            	console.log("添加失败")
			}
		}else{
			console.log("check box is false")
			if($scope.List.del(obj)){
				console.log("删除成功")
			}else{
				console.log("删除失败")
			}
		}
        console.log( $scope.List )
	}
	//删除用户
    $scope.cancleDel = function(){
		$scope.List.clear();
		console.log($scope.List);
		console.log($(":checkbox"))
		//同时将checkbox置为false
		$(":checkbox").each(function(){
			$(this)[0].checked = false;
			console.log($(this))
		})
        $scope.delUserFlag = false;
	}
	$scope.confirmDel = function(){
		console.log("this is confirm btn")
		if(!$scope.List.list.length){
			layer.alert("请勾选要删除的用户");
		}else{
			//删除成功以后执行
			var str = $scope.List.toString();
			delUser(str, $scope.cancleDel)
		}
	}

	/***********************************************/
	console.log(baseurls)
	function delUser(item, fn){
		var url = baseurls + '/' + 'data_mul/user?z_id=' + item;
		console.log(url)
        $http.delete(url).then(
            (res)=>{
                console.log(res);
                if(res.data.ret===1){
                	fn();
                    layer.msg(
                        '用户删除成功',
                        {
                            icon:1,
                            time:2000
                        },
                        function(index){
                            //window.location.href = 'user.html';
                            layer.close(index);
                            $scope.getList();
                        }
                    );
                }else if(res.data.ret===-3){
                    layer.msg(
                        '身份验证失败，请重新登陆..', {
                            icon: 4,
                            time: 2000
                        }
                    );
                    window.location.href = 'login.html';
                }
            },
            (err)=>{
                console.log(err);
                layer.msg(
                    '服务器忙，请稍候再试..', {
                        icon: 4,
                        time: 2000
                    }
                );
            }
		);
	}

	//获取角色列表
	$scope.addobj = {};
    $scope.addobj.roleid = '0';
	$scope.getrole_list = function(){
		$http.get(baseurls+'/select_idname/role?car_role==1').then(function(res){
			if (res.data.ret == 1) {
                $scope.roles = res.data.info;
			} else if (res.data.ret == -3) {
			    window.location.href = 'login.html';
			}
		})
	}
	$scope.getrole_list();
	//作为管理员添加用户
	$scope.addobj = {};
	$scope.show_add = function(){
	    window.location.href = 'add_user.html';
	}
	//add
	$scope.sure_add = function(){
		if($scope.addobj.id == null || $scope.addobj.id == ''){
			$(".user").addClass("has-error");
			return;
		}else{
			$(".user").addClass("has-success");
		}
		$http.post(baseurls+"/data/user",$scope.addobj).then(function(res){
			if(res.data.ret == 1){
				$("#addbox").modal("hide");
				$scope.getList();
			} else if (res.data.ret == -3) {
			    window.location.href = 'login.html';
			}
		});
	}
})