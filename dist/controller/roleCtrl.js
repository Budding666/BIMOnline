var app = angular.module("App", ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller("roleCtrl", function ($scope, $http){
    $scope.go_back = function () {
        window.location.href = 'index1.html';
    };
    //拉取角色信息
    $scope.getRoleList=function(){
        $http.get(baseurls + '/list_nopage/role').then(
            (res)=>{
                console.log(res);
                if(res.data.ret===1){
                    if(res.data.info.length===0){
                        layer.msg(
                            '暂无角色信息，请添加角色信息..', {
                                icon: 7,
                                time: 2000
                            }
                        );
                    }else{
                        $scope.roleList = res.data.info;
                        console.log($scope.roleList)
                    }
                }
            },
            (err)=>{
                console.log(err);
            }
        )
    }
    $scope.getRoleList();
    //删除角色
    $scope.delRole=function(item){
        layer.confirm(
            '您确定要删除该条信息吗？',
            {
                icon:7,
                btn:['确定','取消'],
                yes:function(){
                    $http.delete(baseurls + '/data/role?Id==' + item.id).then(
                        (res) => {
                            console.log(res);
                            if (res.data.ret === 1) {
                                layer.msg(
                                    '角色删除成功', {
                                        icon: 1,
                                        time: 2000
                                    },
                                    function (index) {
                                        $scope.getRoleList();
                                        layer.close(index);
                                    }
                                );
                            } else if (res.data.ret === -3) {
                                layer.msg(
                                    '身份验证失败，请重新登陆..', {
                                        icon: 4,
                                        time: 2000
                                    }
                                );
                                window.location.href = 'login.html';
                            }
                        },
                        (err) => {
                            console.log(err);
                            layer.msg(
                                '服务器忙，请稍候再试..', {
                                    icon: 4,
                                    time: 2000
                                }
                            );
                        }
                    )
                }
            }
        )
    }
    //点击修改 弹出模态框 显示相应标题和按钮
    $scope.editRole=function(item){
        window.location.href = 'editRole.html?' + item.id;
    }
    //跳转到添加角色页
    $scope.goAddRole = function () {
        window.location.href = 'addRole.html';
    };
})