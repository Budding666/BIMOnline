var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('add_proCtrl', function ($scope, $http, $cookieStore) {
	//用于检测的正则变量
	$scope.deviceOrProjectNameRegEN = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]\s]/;
	$scope.deviceOrProjectNameRegCN = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]\s]/;
	$scope.deviceOrProjectNUmber = /^[0-9A-Za-z_-]+$/;
	//返回
	$scope.go_back = function(){
		window.location.href = 'budProject.html';
	};
	//管理员添加项目
	// $scope.op={}
	// $scope.addProInfo=function(){
	// 	if()
	// }
	//add
	$scope.addobj = {};
	$scope.sure_add = function(){
		if ($scope.addobj.name == undefined || $scope.addobj.name == null || $scope.addobj.name == '') {
			$scope.errMsg = '项目名称不能为空';
			return;
		} else if ($scope.deviceOrProjectNameRegEN.test($scope.addobj.name) || $scope.deviceOrProjectNameRegCN.test($scope.addobj.name)) {
			$scope.errMsg = '项目名称不能含有特殊字符和空格';
			return;
		}else if ($scope.addobj.project_num == undefined || $scope.addobj.project_num == null || $scope.addobj.project_num == '') {
			$scope.errMsg = '项目编号不能为空';
		} else if (!$scope.deviceOrProjectNUmber.test($scope.addobj.project_num)) {
			$scope.errMsg = "项目编号只能由数字、字母、下划线、短横杠组成";
			return;
		} else{
			$scope.errMsg=false;
			var $btn = $('#myButton').button('loading')
			$http.post(baseurls + '/data/project', $scope.addobj).then(function (res) {
		    console.log(res.data);
			if(res.data.ret == 1){
				layer.msg(
					"项目添加成功",
					{
						icon:1,
						time:2000
					},
					function(index){
						$btn.button('reset')
						window.location.href = 'budProject.html';
						layer.close(index);
					}
				)
			   
			}else if(res.data.ret==-1){
				if(res.data.info.errcode===1){
					$scope.errMsg = '项目名称或编号已存在';
					$btn.button('reset');
				} else if (res.data.info.errcode === 2){
					$btn.button('reset')
					$scope.errMsg = '项目编号已存在';
				}
			} else if (res.data.ret == -3) {
				layer.msg(
					"身份验证失败，请重新的登录", {
						icon: 4,
						time: 2000
					},
					function (index) {
						window.location.href = 'login.html';
						layer.close(index);
					}
				)
			}
		},
		function(err){
			console.log(err);
			layer.msg(
				"服务器繁忙，请稍候再试..", {
					icon: 4,
					time: 2000
				},
				function (index) {
					layer.close(index);
				}
			)
		}
	);
		}
	}
})