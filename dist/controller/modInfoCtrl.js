var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('modInfoCtrl', function ($scope, $http, $cookieStore) {
    //用于检测的正则变量
    $scope.userCountReg = /^[0-9a-zA-Z]{8,}$/;
    $scope.telephoneNumReg = /^1[3|4|5|7|8]{1}[0-9]{9}$/; //new RegExp("^1[3|4|5|7|8][0-9]{9}$");
    $scope.go_back = function(){
        // window.history.go(-1);
        window.location.href = 'user.html';
    };

    var num = location.href.indexOf("=")
    $scope.id = location.href.slice(num+1)
    $scope.info;
    $scope.getrole_list = function () {
        $http.get(baseurls + '/select_idname/role').then(function (res) {
            if (res.data.ret == 1) {
                $scope.roles = res.data.info;
            } else if (res.data.ret == -3) {
                window.location.href = 'login.html';
            }
        })
    }
    $scope.getrole_list();

    $scope.getinfo_list = function () {
        $http.get(baseurls+'/detail/user?id=='+ $scope.id).then(function (res) {
            if (res.data.ret == 1) {
                $scope.info = res.data.info;
                console.log( $scope.info );

                //let { id, name, pic,   } = $scope.info

                // $scope.newUsrInfo.


                $("#usrPic").css('width', '100px');
                $("#usrPic").css('height', '100px');

                if (res.data.info.pic){
                    $("#usrPic").attr("src", res.data.info.pic);
                }
            } else if (res.data.ret == -3) {
                window.location.href = 'login.html';
            }
        })
    }
    $scope.getinfo_list();

    $('#file').change(function(){
        var file = $(this)[0].files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file)
        reader.onload = function (e) {
            var imgFile = e.target.result;
            $("#usrPic").attr('src', imgFile);
            $("#usrPic").css('width','100px');
            $("#usrPic").css('height', '100px');
        };
    })


    /*************上传用户图片和信息******************/
    $scope.newUsrInfo = null;
    $scope.sure_add = function () {
        if ($scope.info.id == null || $scope.info.id == '' || $scope.info.id == undefined) {
            $scope.errMsg = "用户账号不能为空";
            return;
        } else
        if ($scope.info.name == null || $scope.info.name == '' || $scope.info.name == undefined) {
            $scope.errMsg = "用户昵称不能为空";
            return;
        } else
        if ($scope.info.tele == null || $scope.info.tele == '' || $scope.info.tele == undefined) {
            $scope.errMsg = "用户电话不能为空";
            return;
        } else if (!$scope.telephoneNumReg.test($scope.info.tele)) {
            $scope.errMsg = "请输入正确的电话号码";
            return;
        }else
        if ($scope.info.roleid == null || $scope.info.roleid == '' || $scope.info.roleid == undefined) {
            $scope.errMsg = "用户角色不能为空";
            return;
        } else {
            $scope.errMsg = false;
            var $btn = $('#myButton').button('loading');

            let file = $("#file")[0].files[0];
            if( file ){
                let form = new FormData(); // FormData 对象
                form.append("file", file); // 文件对象

                $.ajax({
                    url:'http://sxzd365.com:9060/photos/upload_shop',
                    type:"POST",
                    async:true,
                    data: form,
                    processData: false,
                    contentType: false,
                    success:function(res){

                        $scope.info.pic = res.result;

                        PostUserInfo($scope.info, $btn);
                    },
                    error:function(err){
                        console.log(err)
                    }
                })

            }else{
                PostUserInfo($scope.info, $btn);
            }
        }
    };


    function PostUserInfo(userinfo, btn){
        $http.put(baseurls+'/data/user?id==' + $scope.id , userinfo).then((res)=>{
            console.log(res.data);
            if(res.data.ret == 1){
                layer.msg(
                    '添加成功！', {
                        icon: 1,
                        title: "提示",
                        time: 2000
                    },
                    function (index) {
                        btn.button('reset')
                        window.location.href='./user.html';
                        layer.close(index);
                    }
                );
            }
            else if(res.data.ret == -1){
                btn.button('reset')
            }
        },(err)=>{
            console.log(err)
        })
    }
})