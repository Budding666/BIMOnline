var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('dailyCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.history.go(-1);
	}
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	console.log(baseurls);

	$scope.getList = function(){
	    $http.get(baseurls + '/list/nopage/perworklog').then(function (res) {
	        console.log(res.data);
			if(res.data.ret == -1){
				$(".dis_none").css("display","none");
			} else if (res.data.ret == 1) {
			    $scope.constList = handle_daily(res.data.info);
			}else if (res.data.ret == -3) {
			    window.location.href = 'login.html';
			}
		});
	};
	$scope.getList();
	$scope.show_add = function(){
		window.location.href="addDaily.html";
	};
    //ɾ����Ŀ
	$scope.show_del = function (obj) {
	    $scope.id = obj.id;
	    $("#del_info").modal("show");
	}
	$scope.sure_del = function () {
	    $http.delete(baseurls + '/data/logwork?id=' + $scope.id).then(function (res) {
	        console.log(res.data);
	        if (res.data.ret == 1) {
	            $scope.getList();
	            $("#del_info").modal("hide");
	        }
	    });
	};
	$scope.show_image = function(obj){
		$scope.imgbig = obj; 
		$("#imgbox").modal("show");
	};
	$scope.hide_image = function(obj){
		$scope.imgbig = obj; 
		$("#imgbox").modal("hide");
	}
	//未通过日志 编辑
	$scope.reEdit = function(dailyId){
		window.location.href="editDaily.html?dailyId="+dailyId;
	}
})