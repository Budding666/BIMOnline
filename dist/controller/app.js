﻿var MyApp = angular.module("App", []);
MyApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);

