var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('edit_threeModalCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
	    window.history.go(-1);
	};
    //edit
	$scope.projectid = getUrlParam("projectid");
	$scope.id = getUrlParam("id");
    //获取要修改的模型
	$http.get(baseurls + '/detail/three_modal?id==' + $scope.id).then(function (res) {
	    console.log(res.data);
	    if (res.data.ret == 1) {
	        $scope.editobj = res.data.info;
	    } else if (res.data.ret == -3) {
	        window.location.href = 'login.html';
	    }
	});
    //管理员确定修改项目
	$scope.sure_edit = function () {
	    if ($scope.editobj.name == undefined || $scope.editobj.name == null || $scope.editobj.name == '') {
	        alert("文件名称不能为空");
	        return;
	    }
	    $http.put(baseurls + '/data/three_modal?id==' + $scope.id, $scope.editobj).then(function (res) {
	        console.log(res.data);
	        if (res.data.ret == 1) {
	            window.location.href = 'budProjectDeti.html?projectid=' + $scope.projectid;
	        } else if (res.data.ret == -3) {
	            window.location.href = 'login.html';
	        }
	    });
	}
	function getUrlParam(key) {
	    //	     获取参数
	    var url = window.location.search;
	    //	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})