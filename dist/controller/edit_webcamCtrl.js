var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('edit_webcamCtrl', function ($scope, $http, $cookieStore) {
	//用于检测的正则变量
	$scope.deviceOrProjectNameRegEN = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]\s]/;
	$scope.deviceOrProjectNameRegCN = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]\s]/;
	$scope.deviceOrProjectNUmber = /^[0-9]{14}$/;
	$scope.go_back = function(){
		// window.history.go(-1);
		window.location.href = 'webcamList.html';		
	};
	$scope.projectid = getUrlParam("projectid");
	$scope.id = getUrlParam("id");
	$scope.getrole_list = function () {
	    $http.get(baseurls + '/select_idname/project').then(function (res) {
	        if (res.data.ret == 1) {
	            $scope.projects = res.data.info;
	        } else if (res.data.ret == -3) {
	            window.location.href = 'login.html';
	        }
	    })
	}
	$scope.getrole_list();
    //获取要修改的摄像头
	$http.get(baseurls + '/detail/device?id==' + $scope.id).then(function (res) {
	    if (res.data.ret == 1) {
	        $scope.editobj = res.data.info;
	    } else if (res.data.ret == -3) {
	        window.location.href = 'login.html';
	    }
	});
	//管理员修改设备
    //edit
	$scope.sure_edit = function () {
	    if ($scope.editobj.devicename == null || $scope.editobj.devicename == '' || $scope.editobj.devicename == undefined) {
	        $scope.errMsg = "设备名称不能为空";
	        return;
	    } else if ($scope.deviceOrProjectNameRegEN.test($scope.editobj.devicename) || $scope.deviceOrProjectNameRegCN.test($scope.editobj.devicename)) {
	    	$scope.errMsg = "设备名称不能含有特殊字符和空格";
	    	return;
	    } else if ($scope.editobj.deviceid == null || $scope.editobj.deviceid == '' || $scope.editobj.deviceid == undefined) {
			$scope.errMsg = "设备编号不能为空";
	        return;
		} else if (!$scope.deviceOrProjectNUmber.test($scope.editobj.deviceid)) {
			$scope.errMsg = "设备编号为14位纯数字组成";
		} else {
			$scope.errMsg = false;
			var $btn = $('#myButton').button('loading')
			$scope.editobj.projectid = $scope.projectid;
			$http.put(baseurls + '/data/device?id=' + $scope.id, $scope.editobj).then(function (res) {
				console.log(res.data);
				if (res.data.ret == 1) {
					layer.msg(
						"设备添加成功", {
							icon: 1,
							time: 2000
						},
						function (index) {
							$btn.button('reset')
							window.location.href = 'webcamList.html?projectid=' + $scope.editobj.projectid;
							layer.close(index);
						}
					)
				} else if (res.data.ret == -1) {
					if (res.data.info.errcode === 1) {
						$scope.errMsg = "设备编号已存在";
						return;
					}
				} else if (res.data.ret == -3) {
					layer.msg(
						"身份验证失败，请重新登陆！", {
							icon: 4,
							time: 2000
						},
						function (index) {
							window.location.href = 'login.html';
							layer.close(index);
						}
					)
				}
			});
		}
	};
	function getUrlParam(key) {
	    //	     获取参数
	    var url = window.location.search;
	    //	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})