var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('userPicCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.history.go(-1);
	};
	console.log(baseurls);
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	$scope.id = getUrlParam("id");

	$scope.get_user = function(){
		console.log($scope.id,$scope.picdata);
        $http.put(baseurls+"/data/user?id=="+$scope.id,$scope.picdata).then(function(res){
            console.log(res.data);
            if(res.data.ret == 1){
                layer.msg('上传成功', {icon: 1});
                setTimeout(function () {
                    window.location.href = 'personal.html';
                }, 2000)

            }else if(res.data.ret == -3){
                window.location.href = 'login.html';
            }
        });
	}

    document.addEventListener("deviceready",onDeviceReady,false);
    //Cordova加载完成会触发
    function onDeviceReady() {
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
    }
    //****************************从相册中选择*********************************
    //获取照片
    function getPhoto(source) {
        //quality : 图像的质量，范围是[0,100]
        navigator.camera.getPicture(
            onPhotoURISuccess,
            function(error)
            {
                console.log("照片获取失败！")
            },
            {
                quality: 50,
                destinationType:destinationType.FILE_URI,
                sourceType: source
            }
        );
    }
    //获取照片成功
    function onPhotoURISuccess(imageURI) {
        //打印出照片路径
        console.log(imageURI);
        //显示照片
        var logImage = document.getElementById('logImage');
        logImage.style.display = 'block';
        logImage.src = imageURI;
        upload(imageURI);
        layer.closeAll()
    }
    //****************************用相机拍照*********************************
    //拍照
    function capturePhoto() {
        //拍照并获取Base64编码的图像（quality : 存储图像的质量，范围是[0,100]）
        navigator.camera.getPicture(
            onPhotoDataSuccess,
            onFail,
            { quality: 50,
                destinationType: destinationType.FILE_URI,
                sourceType: 1
            }
        );
    }
    //拍照成功
    function onPhotoDataSuccess(imageURL) {
        var logImage = document.getElementById('logImage');
        logImage.style.display = 'block';
        logImage.src = imageURL;
        //开始上传
        upload(imageURL);
        layer.closeAll()
    }
    //拍照失败
    function onFail(message) {
        alert('拍照失败: ' + message);
        layer.closeAll()
    }
    //****************************上传文件*********************************
    //上传文件
    // $(".submit_btn").show()
    function upload(fileURL) {
        //上传成功
        var success = function (data) {
            console.log("上传成功! Code = " + data.responseCode);
            var res = JSON.parse(data.response);
            if (res.state === 200) {
                layer.closeAll()
                $scope.picdata = {
                    pic:res.result
                };
                $(".submit_btn").show()
            }
        };
        //上传失败
        var fail = function (error) {
            alert("上传失败! Code = " + error.code);
            layer.closeAll()
        }
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1)+'.jpg';
        options.mimeType = "image/jpeg";
        //上传参数
        var params = {};
        params.value1 = "test";
        params.value2 = "param";
        options.params = params;

        var ft = new FileTransfer();
        //上传地址
        var SERVER = fileurls +'/photos/upload_shop';
        ft.upload(fileURL, encodeURI(SERVER), success, fail, options);
    };
    //****************************拍照或选择图片*********************************

    $scope.userPhoto = function(){
        layer.confirm(
            "请选择照片!",
            {
                btn:['相册','拍照'],
                yes: function () {
                    getPhoto(pictureSource.PHOTOLIBRARY)
                },
                btn2: function () {
                    capturePhoto()
                }
            }
        )
    };

     /*$('.submit_btn').click(function(){
        var data = new FormData($('#submit')[0]);  
//      console.log(data);
        $.ajax({  
            url: fileurls+'/photos/upload_shop',  
            type: 'POST',
            data: data,  
            dataType: 'JSON',
            cache: false,
            processData: false,
            contentType: false,
            success:function(data){
                console.log(data);
                alert("上传成功");
                $scope.picdata = {
                    pic:data.result
                };
				$scope.get_user();
            },
             error:function(data){
                console.log(data);
                alert("上传失败");
            }
        });
     });*/

   
	function getUrlParam(key) {
//	     获取参数
	    var url = window.location.search;
//	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})
