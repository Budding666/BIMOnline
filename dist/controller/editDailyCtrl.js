var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('editDailyCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
	    window.history.go(-1);
	}
	$scope.userid = $cookieStore.get("userid");
    var pictureSource;
    var destinationType;
    document.addEventListener("deviceready",onDeviceReady,false);
    //Cordova加载完成会触发
    function onDeviceReady() {
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
    }
    
    const EDIT_ID = location.search.replace("?dailyId=","");
    console.log(EDIT_ID)
    $http.get(baseurls + '/detail/logwork?id=' + EDIT_ID).then(function (res) {
        console.log(res.data);
        if (res.data.ret == 1) {
            $scope.editobj = res.data.info;
            $scope.editobj.projectid = res.data.info.projectid+""
        }
    });
    //****************************从相册中选择*********************************
    //获取照片
    function getPhoto(){
        //quality : 图像的质量，范围是[0,100]
        navigator.camera.getPicture(
            onPhotoURISuccess,
            function(error)
            {
                console.log("照片获取失败！")
            },
            {
                quality: 50,
                destinationType:destinationType.FILE_URI,
                sourceType: 0
            }
        );
    }
    //获取照片成功
    function onPhotoURISuccess(imageURI) {
        //打印出照片路径
        console.log(imageURI);
        //显示照片
        var logImage = document.getElementById('logImage');
        logImage.style.display = 'block';
        logImage.src = imageURI;

        upload(imageURI);
    }
    //****************************用相机拍照*********************************
    //拍照
    function capturePhoto(){
        //拍照并获取Base64编码的图像（quality : 存储图像的质量，范围是[0,100]）
        navigator.camera.getPicture(
            onPhotoDataSuccess,
            onFail,
            { quality: 50,
                destinationType: destinationType.FILE_URI,
                sourceType: 1
            }
        );
    }
    //拍照成功
    function onPhotoDataSuccess(imageURL) {
        var logImage = document.getElementById('logImage');
        logImage.style.display = 'block';
        logImage.src = imageURL;
        //开始上传
        upload(imageURL);
    }
    //拍照失败
    function onFail(message) {
        alert('拍照失败: ' + message);
    }
    //****************************上传文件*********************************
    //上传文件
    function upload(fileURL) {
        //上传成功
        var success = function (data) {
            layer.msg('上传成功', {icon: 1});
            var res = JSON.parse(data.response);
            if (res.state === 200) {
                $scope.editobj.file_url = res.result
            }
        }
        //上传失败
        var fail = function (error) {
            layer.msg('上传失败', {icon: 1});
        }
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1)+'.jpg';
        options.mimeType = "image/jpeg";
        //上传参数
        var params = {};
        params.value1 = "test";
        params.value2 = "param";
        options.params = params;

        var ft = new FileTransfer();
        //上传地址
        var SERVER = fileurls +'/photos/upload_shop';
        ft.upload(fileURL, encodeURI(SERVER), success, fail, options);
    };
    //****************************拍照或选择图片*********************************
    $scope.logPhoto = function(){
        layer.confirm(
        	"请选择照片!",
			{
				btn:['相册','拍照'],
				yes: function () {
                    getPhoto()
                },
				btn2: function () {
                    capturePhoto()
                }
			}
		)
    };

	$http.get(baseurls+'/select_idname/project').then(function(res){
		if(res.data.ret == 1){
			$scope.constList = res.data.info;
		}else if(res.data.ret == -3){
			window.location.href = 'login.html';
		}
	});
	$scope.change = function () {
	    console.log($scope.myValue)
	}
	$scope.editobj = {};
	/*$('.submit_btn').click(function(){
        var data = new FormData($('#submit')[0]);  
        $.ajax({
            url: fileurls+'/photos/upload_shop',  
            type: 'POST',
            data: data,  
            dataType: 'JSON',
            cache: false,
            processData: false,
            contentType: false,
            success:function(data){
                layer.msg('上传成功', {icon: 1});
                $scope.editobj.file_url = data.result;
                $scope.sure_submit();
            },
             error:function(data){
                layer.msg('上传失败', {icon: 2});
            }
        });      
	});*/
	
    $scope.sure_submit = function(){
        $scope.obj = {
            statue: '-1',
            title: $scope.editobj.title,
            projectid: $scope.editobj.projectid,
            file_url: $scope.editobj.file_url,
            content: $scope.editobj.content
        }
        // console.log($scope.obj)
		$http.put(baseurls+'/data/logwork?id=='+EDIT_ID,$scope.obj).then(function(res){
			// console.log(res.data);
			if(res.data.ret == 1){
				window.location.href = 'daily.html';
			}
		})
	};
})