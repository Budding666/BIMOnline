﻿var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('registerCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.location.href = 'index1.html';
	};
	$scope.addobj = {
        id       : '',
        pass     : '',
        surepass : ''
	};
	$scope.sure_submit = function () {
	    console.log($scope.addobj);
	    var newUserId = $scope.addobj.id === undefined || $scope.addobj.id == null || $scope.addobj.id === '';
	    var newPassWord = $scope.addobj.pass === undefined || $scope.addobj.pass == null || $scope.addobj.pass === '';
	    var reNewPassWord = $scope.addobj.surepass === undefined || $scope.addobj.surepass == null || $scope.addobj.surepass === ''
	    if (newUserId && newPassWord && reNewPassWord) {
            layer.msg('账号和密码不能为空', {icon: 2});
	    } else if(newUserId) {
            layer.msg('账号不能为空', {icon: 2});
	    } else if(newPassWord) {
            layer.msg('密码不能为空', {icon: 2});
	    } else if(reNewPassWord) {
            layer.msg('请再次输入密码', {icon: 2});
	    } else if($scope.addobj.pass.length < 8 && $scope.addobj.surepass.length < 8){
            layer.msg('密码最少八位', {icon: 2});
        } else if($scope.addobj.pass !== $scope.addobj.surepass) {
            layer.msg('两次输入的密码不一致', {icon: 2});
        } else{
            $http.post(baseurls + '/userregist', $scope.addobj).then(function (res) {
                console.log(res.data)
                if(res.data.ret === 1){
                    $scope.addobj = '';
                    layer.msg('注册成功', {icon: 1});
                    setTimeout(function () {
                        self.location = 'login.html'
                    },1300);
                }else if(res.data.ret === -1){
                    layer.msg('账号已存在，换一个试试..!', {icon: 2});
                }else if(res.data.ret === -3){
                    layer.msg('服务器忙，请稍候再试..', {icon: 2});
                }
            })
		}

	};
})