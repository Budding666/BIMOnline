var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('budProjectCtrl', function ($scope, $http, $cookieStore) {
    $scope.go_back = function () {
		window.location.href = 'index1.html';
    };
    $http.get(baseurls + '/session_user').then(function (res) {
        console.log(res.data);
        if (res.data.ret == 1) {
            if ((res.data.user != res.data.fid) && res.data.fid != "0" && res.data.fid != '1') {
                console.log("我是吃瓜群众");
                $("#add_data").css("display", "none");
                $("#edit_data").css("display", "none");
                $("#del_data").css("display", "none");
            }
        }
    })
    $scope.getList = function () {
        console.log(baseurls);
        $http.get(baseurls + '/list_nopage/project').then(function (res) {
            console.log(res.data);
            if (res.data.ret == -1) {
                $(".dis_none").css("display", "none");
            } else if (res.data.ret == 1) {
                $scope.constList = (res.data.info);
            } else if (res.data.ret == -3) {
                window.location.href = 'login.html';
            }
        });
    }
    $scope.getList();
    //管理员添加项目
    //add
    $scope.show_add = function () {
        window.location.href = 'add_pro.html';
    };
    //管理员修改项目
    //edit
    $scope.show_edit = function (obj) {
        window.location.href = 'edit_pro.html?projectid=' + obj.id;
    };
    //删除项目
    $scope.show_del = function (obj) {
        $scope.id = obj.id;
        layer.confirm(
            '您确定要删除该条信息吗？', {
                icon: 7,
                btn: ['确定', '取消'],
                yes: function () {
                    $http.delete(baseurls + '/data/project?id=' + $scope.id).then(
                        (res) => {
                            console.log(res);
                            if (res.data.ret === 1) {
                                layer.msg(
                                    '项目删除成功', {
                                        icon: 1,
                                        time: 2000
                                    },
                                    function (index) {
                                        $scope.getList();
                                        layer.close(index);
                                    }
                                );
                            } else if (res.data.ret === -3) {
                                layer.msg(
                                    '身份验证失败，请重新登陆..', {
                                        icon: 4,
                                        time: 2000
                                    }
                                );
                                window.location.href = 'login.html';
                            }
                        },
                        (err) => {
                            console.log(err);
                            layer.msg(
                                '服务器忙，请稍候再试..', {
                                    icon: 4,
                                    time: 2000
                                }
                            );
                        }
                    )
                }
            }
        )
    }
 
    $scope.watch_3model = function (id) {
        $http.get(baseurls + '/select_idname/three_modal?projectid==' + id + '&type==1').then(function (res) {
            console.log(res.data);
            if (res.data.info == undefined || res.data.info == null || res.data.info == []) {
                $("#three_modal").modal("show");
            } else {
                // window.location.href = "threeModel.html?ifc_json=" + res.data.info.file_url;
            }
        });
    }
    $scope.watch_2model = function (id) {
        $http.get(baseurls + '/select_idname/three_modal?projectid==' + id + '&type==2').then(function (res) {
            console.log(res.data);
            if (res.data.info == undefined || res.data.info == null || res.data.info == []) {
                $("#three_modal").modal("show");
            } else {
                // window.location.href = "threeModel.html?ifc_json=" + res.data.info.file_url;
            }
        });
    }
    $scope.jump_detail = function (projectid) {
        console.log(projectid);
        window.location.href = "budProjectDeti.html?projectid=" + projectid;
    }
});