﻿var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('webcamListCtrl', function ($scope, $http, $cookieStore) {
    $scope.go_back = function () {
		window.location.href = 'index.html';
        // window.history.go(-1);
    }
    $scope.projectid = getUrlParam("projectid");
    console.log($scope.projectid);
    $scope.userid = $cookieStore.get("userid");
    console.log($scope.userid);
    console.log(baseurls);
    $http.get(baseurls + '/session_user').then(function (res) {
        console.log(res.data);
        if (res.data.ret == 1) {
            if ((res.data.user != res.data.fid) && res.data.fid != "0" && res.data.fid != '1') {
                console.log("我是吃瓜群众");
                $("#add_data").css("display", "none"); 
                $("#edit_data").css("display", "none");
                $("#del_data").css("display", "none");
            }
        }
    })
    $scope.getList = function () {
        $http.get(baseurls + '/list_nopage/view_device_project?projectid=' + $scope.projectid).then(function (res) {
            console.log(res.data);
            if (res.data.ret == -1) {
                $(".dis_none").css("display", "none");
            } else if (res.data.ret == 1) {
                if (res.data.info.length <= 0) {
                    $("#modal_info").modal("show");
                }
                $scope.constList = res.data.info;
            } else if (res.data.ret == -3) {
                window.location.href = 'login.html';
            }
        });
    }
    $scope.getList();
    //管理员增加设备
    //add
    $scope.show_add = function () {
        window.location.href = 'add_webcam.html?projectid=' + $scope.projectid;
    };
    //$scope.$watch('deviceready',function(nv){
    //    //$scope.jump1('deviceready');
    //}, false);
    $scope.show_detail = function (obj) {
        console.log(obj.deviceid);
        //跳转到摄像头直播页面(插件)=》传摄像头序列号
        cordova.plugins.myPlugin.coolMethod(obj.deviceid,onSuccess,onFail);
        function onSuccess(message) {
        }
        function onFail(message) {
        }
    }
    $scope.show_edit = function (obj) {
        console.log(obj);
        window.location.href = 'edit_webcam.html?projectid=' + $scope.projectid+'&id='+obj.id;       
    };
    //删除项目
    $scope.show_del = function (obj) {
        $scope.deviceid = obj.deviceid;
        $("#del_info").modal("show");
    }
    $scope.sure_del = function (obj) {
        $http.delete(baseurls + '/data/device?deviceid=' + $scope.deviceid).then(function (res) {
            console.log(res.data);
            if (res.data.ret == 1) {
                $scope.getList();
                $("#del_info").modal("hide");
            }
        });
    };
    function getUrlParam(key) {
        //	     获取参数
        var url = window.location.search;
        //	     正则筛选地址栏
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
        // 匹配目标参数
        var result = url.substr(1).match(reg);
        //返回参数值
        return result ? decodeURIComponent(result[2]) : null;
    }
})