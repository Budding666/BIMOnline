var app = angular.module("App", ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller("editRoleCtrl", function ($scope, $http, $cookieStore) {
    console.log("this is editRole");
    $scope.roleNameRegEN = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]\s]/;
    $scope.roleNameRegCN = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]\s]/;
    $scope.go_back = function () {
        window.location.href = 'role.html';
    };
    // console.log(document.URL)
    //处理ulr获得id
    $scope.roleId=document.URL.split('?')[1];
    // console.log(roleId)
    //拉取要修改的该id下的信息
    $scope.roleInfo={}
    $scope.getRoleInfo=function(){
        $http.get(baseurls + '/detail/role?Id==' + $scope.roleId).then(
            (res)=>{
                if(res.data.ret===1){
                    console.log(res);
                    $scope.roleInfo = res.data.info;
                }
            },
            (err)=>{
                console.log(err);
                layer.msg(
                    '服务器忙，请稍候再试..', {
                        icon: 5,
                        time: 2000
                    },
                    function (index) {
                        layer.close(index);
                    }
                )
            }
        )
    }
    $scope.getRoleInfo();

    $scope.editRoleInfo = function () {
            if ($scope.roleInfo.name === '' || $scope.roleInfo.name === null || $scope.roleInfo.name === undefined) {
                $scope.errMsg = '角色名不能为空';
            } else if ($scope.roleNameRegEN.test($scope.roleInfo.name) || $scope.roleNameRegCN.test($scope.roleInfo.name)) {
                $scope.errMsg = '角色名不能含有特殊字符和空格';
                return;
            } else if ($scope.roleInfo.des === '' || $scope.roleInfo.des === null || $scope.roleInfo.des === undefined) {
                $scope.errMsg = '角色描述不能为空';
            } else {
                var $btn = $('#myButton').button('loading')
                $scope.errMsg = false;
                // $scope.roleInfo.name = $scope.roleName;
                // $scope.op.des = $scope.roleDescription;
                $http.put(baseurls + '/data/role?Id==' + $scope.roleId, $scope.roleInfo).then(
                    (res) => {
                        console.log(res);
                        if (res.data.ret === 1) {
                            layer.msg(
                                '角色修改成功！', {
                                    icon: 1,
                                    time: 2000
                                },
                                function (index) {
                                    window.location.href = 'role.html';                                    
                                    $btn.button('reset')
                                    layer.close(index);
                                }
                            );
                            $scope.roleName = '';
                            $scope.roleDescription = '';
                        }
                        // else if(res.data.ret===-1){
                        //     if(res.data.info.errcode===1){
                        //         $scope.errMsg1 = '角色名称已存在';
                        //         return;
                        //     }
                        // }
                        else if (res.data.ret === -3) {
                            layer.msg(
                                '身份验证失败，请重新登陆..', {
                                    icon: 4,
                                    time: 2000
                                },
                                function (index) {
                                    window.location.href = 'login.html';
                                    layer.close(index);
                                }
                            )
                        } else if (res.data.ret === -4) {
                            layer.msg(
                                '抱歉，您没有权限进行该操作', {
                                    icon: 5,
                                    time: 2000
                                },
                                function (index) {
                                    layer.close(index);
                                }
                            );
                            return;
                        }
                    },
                    (err) => {
                        console.log(err);
                        layer.msg(
                            '服务器忙，请稍候再试..', {
                                icon: 5,
                                time: 2000
                            },
                            function (index) {
                                layer.close(index);
                            }
                        )
                    }
                )
            }
        }
})