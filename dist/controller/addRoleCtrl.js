var app = angular.module("App", ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller("addRoleCtrl", function ($scope, $http, $cookieStore) {
    console.log("this is addRole");
    $scope.roleNameRegEN = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]\s]/;
    $scope.roleNameRegCN = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]\s]/;
    $scope.go_back = function () {
        window.location.href = 'role.html';
        // window.history.go(-1);
    };
    //添加角色
    $scope.op = {}
    $scope.roleName = '';
    $scope.roleDescription = '';
    $scope.addRoleInfo = function () {
        if ($scope.roleName === '' || $scope.roleName === null || $scope.roleName === undefined) {
            $scope.errMsg = '角色名不能为空';
            return;
        } else if ($scope.roleNameRegEN.test($scope.roleName) || $scope.roleNameRegCN.test($scope.roleName)) {
            $scope.errMsg = '角色名不能含有特殊字符和空格';
            return;
        } else if ($scope.roleDescription === '' || $scope.roleDescription === null || $scope.roleDescription === undefined) {
            $scope.errMsg = '角色描述不能为空';
            return;
        } else {
            var $btn = $('#myButton').button('loading')
            $scope.errMsg = false;
            $scope.op.name = $scope.roleName;
            $scope.op.des = $scope.roleDescription;
            $http.post(baseurls + '/data/role', $scope.op).then(
                (res) => {
                    console.log(res);
                    if (res.data.ret === 1) {
                        layer.msg(
                            '角色添加成功！', {
                                icon: 1,
                                time: 2000
                            },
                            function (index) {
                                $btn.button('reset')
                                layer.close(index);
                            }
                        );
                        $scope.roleName = '';
                        $scope.roleDescription = '';
                    }
                    // else if(res.data.ret===-1){
                    //     if(res.data.info.errcode===1){
                    //         $scope.errMsg1 = '角色名称已存在';
                    //         return;
                    //     }
                    // }
                    else if (res.data.ret === -3) {
                        layer.msg(
                            '身份验证失败，请重新登陆..', {
                                icon: 4,
                                time: 2000
                            },
                            function (index) {
                                window.location.href = 'login.html';
                                layer.close(index);
                            }
                        )
                    } else if (res.data.ret === -4) {
                        layer.msg(
                            '抱歉，您没有权限进行该操作', {
                                icon: 5,
                                time: 2000
                            },
                            function (index) {
                                layer.close(index);
                            }
                        );
                        return;
                    }
                },
                (err) => {
                    console.log(err);
                    layer.msg(
                        '服务器忙，请稍候再试..', {
                            icon: 5,
                            time: 2000
                        },
                        function (index) {
                            layer.close(index);
                        }
                    )
                }
            )
        }
    }
})