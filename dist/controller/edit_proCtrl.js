var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('edit_proCtrl', function ($scope, $http, $cookieStore) {
	//用于检测的正则变量
	$scope.deviceOrProjectNameRegEN = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]\s]/;
	$scope.deviceOrProjectNameRegCN = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]\s]/;
	$scope.deviceOrProjectNUmber = /^[0-9A-Za-z_-]+$/;

	$scope.go_back = function(){
	    window.location.href = 'budProject.html';
	};
    //edit
	$scope.projectid = getUrlParam("projectid");
    //获取要修改的项目
	$http.get(baseurls + '/detail/project?id==' + $scope.projectid).then(function (res) {
	    console.log(res.data);
	    if (res.data.ret == 1) {
	        $scope.editobj = res.data.info;
	    } else if (res.data.ret == -3) {
	        window.location.href = 'login.html';
	    }
	});
	//管理员确定修改项目
	$scope.sure_edit = function () {
	    if ($scope.editobj.name == undefined || $scope.editobj.name == null|| $scope.editobj.name == '') {
			$scope.errMsg = '项目名称不能为空';
	        return;
	    } else if ($scope.deviceOrProjectNameRegEN.test($scope.editobj.name) || $scope.deviceOrProjectNameRegCN.test($scope.editobj.name)) {
	    	$scope.errMsg = '项目名称不能含有特殊字符和空格';
	    	return;
	    } else if ($scope.editobj.project_num == undefined || $scope.editobj.project_num == null || $scope.editobj.project_num == '') {
	    	$scope.errMsg = '项目编号不能为空';
	    } else if (!$scope.deviceOrProjectNUmber.test($scope.editobj.project_num)) {
	    	$scope.errMsg = "项目编号只能由数字、字母、下划线、短横杠组成";
	    	return;
	    } else {
	    	$scope.errMsg = false;
			var $btn = $('#myButton').button('loading');
			$http.put(baseurls + '/data/project?id==' + $scope.projectid, $scope.editobj).then(function (res) {
				console.log(res.data);
				if (res.data.ret == 1) {
					layer.msg(
						"项目修改成功", {
							icon: 1,
							time: 2000
						},
						function (index) {
							window.location.href = 'budProject.html';
							layer.close(index);
						}
					)
				} else if (res.data.ret == -1) {
					if (res.data.info.errcode === 1) {
						$scope.errMsg = '项目名称已存在';
						$btn.button('reset');
					} else if (res.data.info.errcode === 2) {
						$btn.button('reset');
						$scope.errMsg = '项目编号已存在';
					}
				} else if (res.data.ret == -3) {
					layer.msg(
						"身份验证失败，请重新登录", {
							icon: 4,
							time: 2000
						},
						function (index) {
							window.location.href = 'login.html';
							layer.close(index);
						}
					)
				}
			});
		}
	}
	function getUrlParam(key) {
	    //	     获取参数
	    var url = window.location.search;
	    //	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})