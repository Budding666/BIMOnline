var app = angular.module("App", ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller("authorityDescriptionCtrl", function ($scope, $http, $cookieStore) {
    $scope.go_back = function () {
        // window.history.go(-1);
        window.location.href = 'index1.html';
    };
    $scope.roles = {};
    $scope.op = {};
    //拉取角色信息
    $scope.getRoleList = function () {
        // console.log(baseurls);
        $http.get(baseurls + '/list_nopage/role').then(
            (res) => {
                console.log("拉取角色信息", res.data)
                if (res.data.ret === 1 && res.data.info.length > 0) {
                    if (res.data.info.length === 0) {
                        layer.msg(
                            '暂无角色信息，请添加角色信息..', {
                                icon: 7,
                                time: 2000
                            }
                        );
                    } else {
                        $scope.roles = res.data.info;
                        console.log($scope.roles);

                    }
                }
            },
            (err) => {
                console.log(err);
            }
        )
    };
    $scope.getRoleList();

    //获取当前用户的所有项目
    $scope.allpower = [];
    $scope.projects = {};
    $http.get(baseurls + "/list_nopage/project").then(function (res) {
        // console.log("获取当前用户的所有项目", res.data.info);
        if (res.data.ret == 1) {
            if(res.data.info.length===0){
                layer.msg(
                    '暂无项目信息，请添项目..', {
                        icon: 7,
                        time: 2000
                    }
                );
            }else{
                $scope.projects = res.data.info;
                var arr = res.data.info;
                for (var i = 0; i < arr.length; i++) {
                    var projectId = arr[i].id;
                    var obj = {
                        projectid: projectId
                    }
                    $scope.allpower.push(obj);
                }
            }
        }
    });
    $scope.role_id = 'no';
    $scope.sel_rol = function(){
        console.log($scope.role_id);
        $(".pro_checked").prop("checked",false);
        if ($scope.role_id === 'no') {

        }else{
            $http.get(baseurls+"/detail/role?id=="+$scope.role_id).then(function(res){
                // console.log(res.data);
                var power = res.data.info.power;
                set_value(power);
            });

        }       

    };

    function set_value(data){
            if (data == null || data === '' || data === '0' || data === 'no') {
                return;
            }
            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++) {
                var pid = data[i].projectid;
                var power = 0;
                power += data[i].power;
                var checkboxs = $(".project"+pid);
                var t1 = (power & 1) == 1?true:false;
                var t2 = (power & 2) == 2?true:false;
                var t3 = (power & 4) == 4?true:false;
                $(checkboxs[0]).prop("checked",t1);
                $(checkboxs[1]).prop("checked",t2);
                $(checkboxs[2]).prop("checked",t3);
            }

    }
    // 全选
    $scope.selectAll = function (master) {
        if (master == true) {
            $scope.master = true;
        } else {
            $scope.master = false;
        }
    }

    $scope.givePermissions = function () {
        if ($scope.role_id === 'no') {
            $("#infoAlert").modal("show");
            return;
        } else {
            console.log($scope.allpower);
            for (var i = 0; i < $scope.allpower.length; i++) {
                var projectid = $scope.allpower[i].projectid;
                var checkboxs = $(".project" + projectid);
                var power = 0;
                for (var j = 0; j < checkboxs.length; j++) {
                    if ($(checkboxs[j]).is(':checked')) {
                        switch (j) {
                            case 0:
                                power += 1;
                                break;
                            case 1:
                                power += 2;
                                break;
                            case 2:
                                power += 4;
                                break;
                        }
                    } //if
                }
                $scope.allpower[i].power = power;
            }
            var obj = {power: JSON.stringify($scope.allpower)};
           
            $http.put(baseurls + '/data/role?id==' + $scope.role_id, obj).then(function (res) {
                console.log(res.data);
                if (res.data.ret == 1) {
                    layer.msg('提交成功', {icon: 1});

                }
            });


        }
    }


});