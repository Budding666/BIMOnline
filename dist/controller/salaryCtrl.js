var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('salaryCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.history.go(-1);
	}
	console.log(baseurls);
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	$scope.getList = function(){
	    $http.get(baseurls + '/list_nopage/project').then(function (res) {
	        console.log(res.data);
			if(res.data.ret == -1){
				$(".dis_none").css("display","none");
			}else if(res.data.ret == 1){
				$scope.constList = res.data.info;
			} else if (res.data.ret == -3) {
			    window.location.href = 'login.html';
			}
		});
	}
	//$scope.getList();
	$scope.show_modal = function (obj) {
	    window.location.href = 'budProjectDeti.html?projectid=' + obj.id;  
	}
})