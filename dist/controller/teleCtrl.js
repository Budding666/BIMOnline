var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('teleCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.history.go(-1);
	}
	console.log(baseurls);
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	$scope.id = getUrlParam("id");
	//获取验证码时 倒计时
	$("#get_ver").click(function(){
      	var disabled = $("#get_ver").attr("disabled");
      	if(disabled){
        	return false;  
      	}
      	if($(".input_tele").val() == "" || isNaN($(".input_tele").val()) || $(".input_tele").val().length != 11 ){  
       		$("#box").modal("show");
       		return false;  
      	}
		$http.post('http://www.sxzd365.com:9066/regist_xnsjy',$scope.addobj,{'withCredentials':false}).then(function(res){
      		console.log(res.data);
      		settime();
      		if(res.data.ret == 1){
			}else if(res.data.ret == -3){
      			window.location.href = 'login.html';
      		}
		});
    });
	var countdown=60;
	var generate_code = $("#get_ver");
    function settime() {
        if (countdown == 0) {
	        generate_code.attr("disabled",false);
	        generate_code.val("获取验证码");
	        countdown = 60;
	        return false;
        } else {
	        generate_code.attr("disabled", true);
	        generate_code.val("重新发送(" + countdown + ")");
	        countdown--;
      	}
      	setTimeout(function() {
        	settime();
      	},1000);
    }
    //提交修改手机号
    $scope.addobj = {};
	$scope.addobj.code=123456;
	$scope.sure_submit = function(){
		if($(".input_tele").val() == "" || isNaN($(".input_tele").val()) || $(".input_tele").val().length != 11 ){  
       		$("#box").modal("show");
       		return false;  
      	}
		if($scope.addobj.code.length !=6 || $scope.addobj.code !=123456){
			$(".sure_ver").val("验证码错误");
			$(".ver_code").addClass("has-error");
			return;
		}else{
			$(".ver_code").addClass("has-success");
		}
		delete $scope.addobj.code;
		console.log($scope.addobj);
		$scope.addobj.tele = $scope.addobj.phone;
		$http.put(baseurls+'/data/user?id=='+$scope.id,{tele:$scope.addobj.phone}).then(function(res){
			console.log(res.data);
			if(res.data.ret == 1){
				window.location.href = 'personal.html';
			}
			if(res.data.ret == -3){
				window.location.href = "login.html";
			}
		});
	};
    //获取地址栏参数
	function getUrlParam(key) {
//	     获取参数
	    var url = window.location.search;
//	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})
