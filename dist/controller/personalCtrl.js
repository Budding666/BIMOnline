var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('personalCtrl', function ($scope, $http, $cookieStore) {
	//退出登录
	$scope.logout=function(){
		$http.get(baseurls + '/logout').then(
			(res)=>{
				console.log(res);
				if(res.data.ret===-3){
					window.location.href = 'login.html'
				}
			},
			(err)=>{
				console.log(res);
			}
		)
	}
	$scope.go_back = function(){
		window.history.go(-1);
	}
	$scope.go_index = function () {
	    window.location.href = 'index.html';
	}
	$scope.go_personal = function () {
	    window.location.href = 'personal.html';
	}
	$scope.go_salarry = function () {
	    window.location.href = 'salary.html';
	}
	$scope.go_webcam = function () {
	    window.location.href = 'webcamPro.html';
	}
	$scope.jump_userPic = function(id){
		window.location.href = 'userPic.html?id='+id;
	};
	$scope.jump_pass = function(id){
		window.location.href = 'pass.html?id='+id;
	};
	$scope.jump_daily = function(id){
		window.location.href = 'daily.html?id='+id;
	};
	$scope.jump_tele = function(id){
		window.location.href = 'tele.html?id='+id;
	};
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	console.log(baseurls);
		//获取用户列表
	$http.get(baseurls + '/detail/user?id==' + $scope.userid).then(function (res) {
		console.log(res);
		if (res.data.ret == 1) {
		    if (res.data.info.pic == null) {
		        res.data.info.pic = 'dist/images/user/user_big.png';
		    }
		    $scope.userInfo = res.data.info;
		} else if (res.data.ret == -1) {
		    $(".dis_none").css("display", "none");
		} else if (res.data.ret == -3) {
		    window.location.href = 'login.html';
		}
	});
})