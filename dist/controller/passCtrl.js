var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('passCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.history.go(-1);
	}
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	console.log(baseurls);
	$scope.id = getUrlParam("id");
	$scope.data = {};

    //密码验证
    function isPassword(pass){
        var str = pass;
        if(str==null || str.length<7 || str.length>21){
            return false;
        }
        var reg1 = new RegExp(/^[0-9A-Za-z]+$/);
        if (!reg1.test(str)) {
            return false;
        }else{
            return true;
        }
    }

    $scope.sure_submit = function () {
        console.log($scope.data);
        if($scope.data.oldpasswd == null || $scope.data.oldpasswd == ''){
            // $scope.errMsg = '请输入旧密码';
            layer.msg('请输入旧密码', {icon: 2});
            return;
        }
        if($scope.data.newpasswd1 == null || $scope.data.newpasswd1 == '' || !isPassword($scope.data.newpasswd1)){
            // $scope.errMsg = '请输入正确格式的新密码';
            layer.msg('请输入正确格式的新密码', {icon: 2});
            return;
        }
        if($scope.data.newpasswd2 == null || $scope.data.newpasswd2 == '' || !isPassword($scope.data.newpasswd2)){
            layer.msg('请确认新密码', {icon: 2});
            // $scope.errMsg = '请确认新密码';
            return;
        }
        if($scope.data.newpasswd1 != $scope.data.newpasswd2){
            layer.msg('两次新密码输入不一致', {icon: 2});
            // $scope.errMsg = '两次新密码输入不一致';
            return;
        }

        //当旧密码正确 并且两次输入的新密码相同时
        //修改密码
        $http.put(baseurls+"/modifypass?id=="+$scope.id,{pass: $scope.data.newpasswd2,oldpass:$scope.data.oldpasswd}).then(function(res){
            console.log(res.data.ret);
            if(res.data.ret == 1){
                layer.msg('密码修改成功', {icon: 1});
                setTimeout(function () {
                    window.location.href =  "login.html";
                }, 3000)
            } else if (res.data.ret == -3) {
                layer.msg('身份验证失败重新登录', {icon: 1});
                setTimeout(function () {
                    window.location.href = 'login.html';
                }, 3000)

            }else{
                layer.msg('原密码不正确', {icon: 2});
                // $scope.errMsg = '旧密码不正确';
            }
        }) 
    }


    function getUrlParam(key) {
//	     获取参数
	    var url = window.location.search;
//	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})
