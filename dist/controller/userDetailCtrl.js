var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('userDetailCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.history.go(-1);
	}
	console.log(baseurls);
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	$scope.id = getUrlParam("id");
	//获取用户列表
	$scope.getList = function(){
		$http.get(baseurls+'/detail/user?id=='+$scope.id).then(function(res){
			console.log(res);
			if (res.data.ret == 1) {		
				if(res.data.info.pic == null){
					res.data.info.pic = 'dist/images/user/user_big.png';
				}
				$scope.constList =res.data.info;
				//发送角色id 获取角色信息
				$http.get(baseurls + '/detail/role?Id==' + res.data.info.roleid).then(
					(res) => {
						if (res.data.ret === 1) {
							console.log(res);
							$scope.roleName = res.data.info.name;
						}
					},
					(err) => {
						console.log(err);
						layer.msg(
							'服务器忙，请稍候再试..', {
								icon: 5,
								time: 2000
							},
							function (index) {
								layer.close(index);
							}
						)
					}
				)
			}else if(res.data.ret == -1){
				$(".dis_none").css("display","none");
			}else if(res.data.ret == -3){
				window.location.href = 'login.html';
				return;
			}
		})
	}
    $scope.modify = function(id){
        window.location.href = 'modInfo.html?id=' + id;
	}
	$scope.getList();
	function getUrlParam(key) {
//	     获取参数
	    var url = window.location.search;
//	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})
