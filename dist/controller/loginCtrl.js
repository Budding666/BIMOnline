﻿var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('loginCtrl', function ($scope, $http, $cookieStore) {
	localStorage.removeItem('car_role');

	// $scope.go_back = function(){
	// 	window.history.go(-1);
	// };
	// var reg = /^\d{n}$/;
    $scope.user= {
        id:'',
        pass:''
    }

    //账号 密码同步显示
    $scope.$watch("user.id", function () {
        if($scope.user.id == '' ){
            $scope.user.pass = ''
        }
    });

    //记住账号密码后直接登录
    $scope.login_name = $cookieStore.get('userid');
    $scope.login_pass = $cookieStore.get('userpass');
    if($scope.login_name&&$scope.login_pass){
        $scope.user.id = $scope.login_name;
        $scope.user.pass = $scope.login_pass
        $scope.user.pass = $scope.login_pass
    }else{
        $scope.user= {
            id:'',
            pass:''
        }
    }

	$scope.sure_submit = function () {
        var loginId = $scope.user.id === undefined || $scope.user.id === null || $scope.user.id === '';
       	var loginPassword =  $scope.user.pass === undefined || $scope.user.pass == null || $scope.user.pass === '' ;
        if ( loginId && loginPassword) {
            layer.msg('账号和密码不能为空', {icon: 2});
	    } else if(loginId) {
            layer.msg('账号不能为空', {icon: 2});
		} else if(loginPassword){
            layer.msg('密码不能为空', {icon: 2});
		} else if($scope.user.pass.length < 8){
            layer.msg('密码最少八位', {icon: 2});
        } else {
			console.log(baseurls + "/userlogin");
            $http.post(baseurls + "/userlogin", $scope.user).then(function (res) {
                console.log(res.data);
                if (res.data.ret == 1) {
                    localStorage.setItem('car_role', res.data.info.car_role);
                    $cookieStore.put("userid", res.data.info.id);
                    $cookieStore.put("userpass", $scope.user.pass);

                    $cookieStore.put("fid", res.data.info.fid);
                    localStorage.setItem('fid', res.data.info.fid);
                    if (res.data.info.fid == 1) {
                       window.location.href = 'index.html';
                    } else if (res.data.info.car_role == 1) {
                       window.location.href = 'index1.html';
                    } else if (res.data.info.car_role == 2) {
                       window.location.href = 'CarManage/index.html';
                    }

                } else if (res.data.ret == -1) {
                    layer.msg('账号或密码错误', {icon: 2});
                }
            })
		}
	};

	$scope.jump_register = function () {
	    window.location.href = 'register.html';
	}
	// //init
    // $scope.user = {
    // 	id   : '',
	// 	pass : ''
	// };

	// $scope.sure_submit = function () {
    //     var loginId = $scope.user.id === undefined || $scope.user.id === null || $scope.user.id === '';
    //    	var loginPassword =  $scope.user.pass === undefined || $scope.user.pass == null || $scope.user.pass === '' ;
    //     if ( loginId && loginPassword) {
	//         return;
	//     }

	//     if (loginId) {
	//         return;
	//     }
	//     if (loginPassword) {
	//         return;
	//     }
	//     $http.post(baseurls + "/userlogin", $scope.user).then(function (res) {
	//         console.log(res.data);
	//         if (res.data.ret == 1) {
	// 			$cookieStore.put("userid",res.data.info.id);
	// 			localStorage.setItem('car_role', res.data.info.car_role);
	// 			// if (res.data.info.car_role === "1") {
	// 			// 	window.location.href = "#/home"; //跳转到实时数据页面
	// 			// } else if (res.data.info.car_role === "2") {
	// 			// 	window.location.href = "#/home2";
	// 			// }
	// 			window.location.href = 'index1.html';
	//         } else if (res.data.ret == -1) {
	//             alert("账号或密码错误");
	//         }
	// 	})
	// };
	// $scope.jump_register = function () {
	//     window.location.href = 'register.html';
    // }
   // $('.submit_btn').click(function(){
       // var data = new FormData($('#submit')[0]);

       //  $.ajax({
       //      url: baseurls+'/userlogin',
       //      type: 'POST',
       //      data: $scope.user,
       //
       //      cache: false,
       //      processData: false,
       //      contentType: false,
       //      success:function(data){
       //          console.log(data);
       //      },
       //       error:function(data){
       //           console.log('error',data);
       //      }
       //  });
	});
//})