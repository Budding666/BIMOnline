var app = angular.module("App", ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller("index1Ctrl", function ($scope, $http, $cookieStore) {
	$scope.go_index = function(){
		window.location.href = 'index.html';
	}
	$scope.go_personal = function(){
		window.location.href = 'personal.html';
	}
	$scope.go_salarry = function () {
		window.location.href = 'salary.html';
	}
	$scope.go_webcam = function () {
	    window.location.href = 'webcamPro.html';
	}
	$scope.userid = $cookieStore.get("userid");
	console.log('$scope.userid is ', $scope.userid);
	$http.get(baseurls + '/session_user').then(function (res) {
	    console.log(res.data);
	    if (res.data.ret == 1) {
	        if((res.data.user != res.data.fid) && res.data.fid != "0"  && res.data.fid != '1'){
				console.log("我是吃瓜群众");
	           $(".none_div" + 6).css("display", "none");				
	           $(".none_div" + 7).css("display", "none");
			   $(".none_div" + 8).css("display", "none");
	           $(".none_div" + 9).css("display", "none");
	        }
	    }
	})
	$scope.iconList = [
		{ id: 1, img: "icon_01.png", name: "项目", url: "budProject.html" },
        { id: 2, img: "icon_10.png", name: "摄像头", url: "webcamPro.html" },
        { id: 3, img: "icon_11.png", name: "三维模型", url: "modalPro.html" },
        { id: 4, img: "icon_03.png", name: "日志", url: "daily.html" },
        //{ id: 5, img: "icon_11.png", name: "车管家", url: "CarManage/index.html" },
        // { id: 5, img: "icon_12.png", name: "车管家", url: "" },
	    { id: 5, img: "icon_13.png", name: "考勤", url: "salary.html" },
        { id: 6, img: "icon_05.png", name: "审核", url: "check.html" },
		{ id: 7, img: "icon_06.png", name: "用户", url: "user.html" },
        { id: 8, img: "role.png", name: "角色", url: "role.html" },
        { id: 9, img: "Authoritydescription.png", name: "权限", url: "authorityDescription.html" },


	];
	$scope.jump_register = function () {
	    window.location.href = 'register.html';
	}
});