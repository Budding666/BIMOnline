var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('add_webcamCtrl', function ($scope, $http, $cookieStore) {
	//用于检测的正则变量
	$scope.deviceOrProjectNameRegEN = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]]/;
	$scope.deviceOrProjectNameRegCN = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]]/;
	$scope.deviceOrProjectNUmber = /^[0-9]{14}$/;
	$scope.go_back = function(){
		// window.history.go(-1);
		window.location.href = 'webcamList.html';
	};
	$scope.getrole_list = function () {
	    $http.get(baseurls + '/select_idname/project').then(function (res) {
	        if (res.data.ret == 1) {
	            $scope.projects = res.data.info;
	        } else if (res.data.ret == -3) {
	            window.location.href = 'login.html';
	        }
	    })
	}
	$scope.getrole_list();
	//管理员添加设备
    //add
	$scope.projectid = getUrlParam("projectid");
	$scope.addobj = {};
	$scope.sure_add = function () {
	    if ($scope.addobj.devicename == null || $scope.addobj.devicename == '' || $scope.addobj.devicename == undefined) {
	    	$scope.errMsg = "设备名称不能为空";
	    	return;
	    } else if ($scope.deviceOrProjectNameRegEN.test($scope.addobj.devicename) || $scope.deviceOrProjectNameRegCN.test($scope.addobj.devicename)) {
			$scope.errMsg = "设备名称不能含有特殊字符和空格";
	    	return;
		}else if ($scope.addobj.deviceid == null || $scope.addobj.deviceid == '' || $scope.addobj.deviceid == undefined) {
	        $scope.errMsg = "设备编号不能为空";
	        return;
	    } else if (!$scope.deviceOrProjectNUmber.test($scope.addobj.deviceid)) {
			$scope.errMsg = "设备编号为14位纯数字组成";
		}else{
			$scope.errMsg=false;
			var $btn = $('#myButton').button('loading');
			$scope.addobj.projectid = $scope.projectid;
			$http.post(baseurls + '/data/device', $scope.addobj).then(function (res) {
				console.log(res.data);
				if (res.data.ret == 1) {
					layer.msg(
						"设备添加成功", {
							icon: 1,
							time: 2000
						},
						function (index) {
							$btn.button('reset');
							window.location.href = 'webcamList.html?projectid=' + $scope.addobj.projectid;
							layer.close(index);
						}
					)
				} else if (res.data.ret == -1) {
					if(res.data.info.errcode===1){
						$btn.button('reset')
						$scope.errMsg = "设备编号已存在";
						return;
					}
				} else if (res.data.ret == -3) {
					layer.msg(
						"身份验证失败，请重新登陆！", {
							icon: 4,
							time: 2000
						},
						function (index) {
							window.location.href = 'login.html';
							layer.close(index);
						}
					)
				}
			});
		}
	};
	function getUrlParam(key) {
	    //	     获取参数
	    var url = window.location.search;
	    //	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})