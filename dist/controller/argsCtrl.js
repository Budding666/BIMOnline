var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('argsCtrl', function ($scope, $http, $cookieStore) {
	$scope.go_back = function(){
		window.history.go(-1);
	}
	$scope.userid = $cookieStore.get("userid");
	console.log($scope.userid);
	console.log(baseurls);
	
	$scope.deviceid = getUrlParam("deviceid");
	$scope.refresh = function(){
		window.location.reload();
	}
	//下发参数
	$scope.send_data = {};
	$scope.send_data.deviceid = $scope.deviceid;
	console.log($scope.send_data);
	$scope.send_args = function(){
		$http.post(baseurls+'/query_device',$scope.send_data).then(function(res){
             console.log(res.data);
             if(res.data.ret == -3){
             	window.location.href = 'login.html';
             }else if(res.data.ret == 1){
             	$scope.constList= res.data.info;
             }
        });
	}
	
	function getUrlParam(key) {
//	     获取参数
	    var url = window.location.search;
//	     正则筛选地址栏
	    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	    // 匹配目标参数
	    var result = url.substr(1).match(reg);
	    //返回参数值
	    return result ? decodeURIComponent(result[2]) : null;
	}
})
