var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('budProjectDetiCtrl', function ($scope, $http, $cookieStore) {
    $scope.go_back = function () {
        // window.location.href = "modalPro.html";
        window.history.go(-1);
    };
    //弹出提示上传3d模型
    $scope.userid = $cookieStore.get("userid");
    console.log($scope.userid);
    console.log(baseurls);
    $scope.projectid = getUrlParam("projectid");
    $scope.getList = function () {
        $http.get(baseurls + '/list_nopage/three_modal?projectid==' + $scope.projectid).then(function (res) {
            console.log(res.data);
            if (res.data.ret == -1) {
                $(".dis_none").css("display", "none");
            } else if (res.data.ret == 1) {
                if (res.data.info.length <= 0) {
                      layer.confirm(
                        '请先在PC端上传3D模型', {
                            icon: 7,
                            btn: ['确定'],
                            yes: function (index) {
                                layer.close(index)
                            }
                        }
                    )
                }
                $scope.constList = res.data.info;
            } else if (res.data.ret == -3) {
                window.location.href = "login.html";
            }
        });
    }
    $scope.getList();
    //管理员修改项目
    //edit
    $scope.show_edit = function (obj) {
        console.log(obj);
        window.location.href = 'edit_three_modal.html?projectid=' + obj.projectid+'&id='+obj.id;
    };
    //删除项目
    $scope.show_del = function (obj) {
        $scope.id = obj.id;
        $("#del_info").modal("show");
    }
    $scope.sure_del = function () {
        $http.delete(baseurls + '/data/three_modal?id=' + $scope.id).then(function (res) {
            console.log(res.data);
            if (res.data.ret == 1) {
                $scope.getList();
                $("#del_info").modal("hide");
            }
        });
    };
    //管理员修改项目
    //edit
    //根据类型id 进入对应页面(3D,2D)
    $scope.jump_detail = function (obj) {
        if (obj.type == '1') {
            window.location.href = "threeModal.html?json==" + obj.json_url;
        } else if (obj.type == '2') {
            window.location.href = "twoModal.html?file==" + obj.file_url;
        }
    };

    //查看2D图纸
    $scope.watch_2model = function (id) {
        $http.get(baseurls + '/select_idname/three_modal?projectid==' + id + '&type==2').then(function (res) {
            console.log(res.data);
            if (res.data.info == undefined || res.data.info == null || res.data.info == []) {
                $("#three_modal").modal("show");
            } else {
                // window.location.href = "threeModel.html?ifc_json=" + res.data.info.file_url;
            }
        });
    }

    function getUrlParam(key) {
        //	     获取参数
        var url = window.location.search;
        //	     正则筛选地址栏
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
        // 匹配目标参数
        var result = url.substr(1).match(reg);
        //返回参数值
        return result ? decodeURIComponent(result[2]) : null;
    }
});