var app = angular.module('App', ['ng', 'ngCookies']);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
app.controller('webcamProCtrl', function ($scope, $http, $cookieStore) {
    $scope.go_back = function () {
        // window.history.go(-1);
        window.location.href = 'webcamList.html';
    }
    console.log(baseurls);
    $scope.userid = $cookieStore.get("userid");
    console.log($scope.userid);
    $scope.getList = function () {
        $http.get(baseurls + '/list_nopage/project').then(function (res) {
            console.log(res.data);
            if (res.data.ret == -1) {
                $(".dis_none").css("display", "none");
            } else if (res.data.ret == 1) {
                $scope.constList = res.data.info;
            } else if (res.data.ret == -3) {
                window.location.href = 'login.html';
            }
        });
    };
    $scope.getList();
    $scope.show_detail = function (id) {
        console.log(id);
        window.location.href = 'webcamList.html?projectid=' + id;
    }
});
